<?php

class DataBaseAccess
{
	///// DATABASE CONFIGURATION /////
	
	//DEV
	private $server = "localhost";
	private $user = 'CHANGEHERE';
	private $pass = 'CHANGEHERE';
	private $db = 'publicticketsystem';


	static private $instance;

	private function __contructor() {

	}

	public function __clone()
	{
		trigger_error('Clone is not allowed.', E_USER_ERROR);
	}

	static public function getInstance() {
		if (!isset(self::$instance)) {
			$c = __CLASS__;
			self::$instance = new $c;
		}

		return self::$instance;
	}
	
	private function openConnection() {
		$mysqli = new mysqli($this->server, $this->user, $this->pass, $this->db);
		return $mysqli;
	}
	
	private function closeConnection($db) {
		$db->close();
	}

	private function runQuery($sql) {

		$mysqli = new mysqli($this->server, $this->user, $this->pass, $this->db);

		if (mysqli_connect_errno()) trigger_error(mysqli_connect_error());
		$mysqli->set_charset("iso-8859-1");
		$result = $mysqli->query($sql) or trigger_error($mysqli->error."[$sql]");
		$mysqli->close();
		return $result;

	}
	///MODULE ///
	public function listAllTickets() {
		$sql = "SELECT t.t_id, t.uuid, t.price, z.name, t.price, t.pubKey, t.validTime 
				FROM ticket t, zone z, ticket_type tp
				WHERE `t_id` = `z.id` AND `t_id` = `tp.id` ORDER BY t.t_id;";
		$result = $this->runQuery($sql);
	
		return $result;
	}
	
	public function listTicketByID($id) {
		$sql = "SELECT t.t_id, t.uuid, t.zone, t.price, t.pubKey, t.validTime 
		FROM ticket t
		WHERE t.t_id = '$id'; ";
		$result = $this->runQuery($sql);
		return $result;
	}
	
	public function insertTicket($uuid, $zone, $price, $pubKey, $validTime) {
		$result = false;
		$conn = $this->openConnection();
		$pstmt = $conn->prepare("INSERT INTO ticket (uuid, zone, price, pubKey, validTime)
				VALUES (?, ?, ?, ?, ?)");
		$pstmt->bind_param('siisi',$uuid, $zone, $price, $pubKey, $validTime);
		if ($pstmt->execute()) {
			$result = true;
		}
		$this->closeConnection($conn);
		return $result;
	}
	
	public function deleteTicket($t_id) {
		$result = false;
		$conn = $this->openConnection();
		$pstmt = $conn->prepare("DELETE FROM ticket WHERE t_id = ?");
		$pstmt->bind_param('i',$t_id);
		if ($pstmt->execute()) {
			$result = true;
		}
		$this->closeConnection($conn);
		return $result;
	}
	
	public function updateTicket($id, $uuid, $zone, $price, $pubKey, $validTime) {
		$result = false;
		$conn = $this->openConnection();
	
		$pstmt = $conn->prepare("UPDATE ticket SET
				uuid = ?, zone = ?, price = ?, pubKey = ?, validTime = ?
				WHERE t_id = ?");
		$pstmt->bind_param('siisi',$uuid, $zone, $price, $pubKey, $validTime);
	
		if ($pstmt->execute()) {
			$result = true;
		}
		$this->closeConnection($conn);
		return $result;
	}
	
	
	/// USER ///
	public function loginUser($email, $pass) {
		$sql= "SELECT u_id FROM user WHERE email = '$email' AND password = '$pass';";
		$result = $this->runQuery($sql);
		return $result;
	}
	
	public function getProfile($email) {
		$sql = "SELECT u_id, email, name, last_name FROM user WHERE email = '$email';";
		$result = $this->runQuery($sql);
		return $result;
	}

	public function userDataByEmail($email) {
		$sql= "SELECT u_id, email, name, last_name FROM user WHERE email = '$email';";
		$result = $this->runQuery($sql);
		return $result;
	}
	
	public function registerUser($name, $lastName, $email, $password) {
		$result = false;
		$conn = $this->openConnection();
	
		$pstmt = $conn->prepare("INSERT INTO user (name, last_name, email, password) VALUES (?, ?, ?, ?)");
		$pstmt->bind_param('ssss',$name, $lastName, $email, $password);
	
		if ($pstmt->execute()) {
			$result = true;
		}
		$this->closeConnection($conn);
		return $result;
	}
	
	public function updateUser($name, $lastName, $email) {
		$result = false;
		$conn = $this->openConnection();
	
		$pstmt = $conn->prepare("UPDATE user SET name = ?, last_name = ? WHERE email = ?");
		$pstmt->bind_param('sss',$name, $lastName, $email);
	
		if ($pstmt->execute()) {
			$result = true;
		}
		$this->closeConnection($conn);
		return $result;
	}
	
}

?>
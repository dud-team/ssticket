<?php

class Ticket {
	
	private $t_id;
	private $uuid;
	private $zone;
	private $price;
	private $pubKey;
	private $validTime;
	
	public function __construct($t_id, $uuid, $zone, $price, $pubKey, $validTime) {
		$this->t_id =  $t_id;
		$this->uuid = $uuid;
		$this->zone = $zone;
		$this->price = $price;
		$this->pubKey = $pubKey;
		$this->validTime = $validTime;
	}
	
	public function __destruct() {
		unset($this->t_id);
		unset($this->uuid);
		unset($this->zone);
		unset($this->price);
		unset($this->pubKey);
		unset($this->validTime);
	}
	
	public function getId() {
		return $this->t_id;
	}
	
	public function getUuid() {
		return $this->uuid;
	}
	
	public function getZone() {
		return $this->zone;
	}
	
	public function getPrice() {
		return $this->price;
	}
	
	public function getPublicKey() {
		return $this->pubKey;
	}
	
	public function getValidTime() {
		return $this->validTime;
	}
	
	public function setID($t_id) {
		$this->t_id = $t_id;
	}
	
	public function setUuid($uuid) {
		$this->uuid = $uuid;
	}
	
	public function setZone($zone) {
		$this->zone = $zone;
	}
	
	public function setPrice($price) {
		$this->price = $price;
	}
	
	public function setValidTime($validTime) {
		$this->validTime = $validTime;
	}
 	
	
}

?>
<?php
require_once $_SERVER['DOCUMENT_ROOT']."/PublicTicketSystemServer/src/controller/TicketController.class.php";


$t_id = $_GET['t_id'];
$obj1 = TicketController::getInstance();

$result = $obj1->getTicketByID($t_id);
header('Content-type: application/json charset=UTF-8');
echo json_encode($result);


<?php
require_once $_SERVER['DOCUMENT_ROOT']."/PublicTicketSystemServer/src/controller/TicketController.class.php";


$uuid = $_GET['uuid'];
$zone = $_GET['zone'];
$price = $_GET['price'];
$pubKey = $_GET['pubKey'];
$validTime = $_GET['validTime'];

$obj1 = TicketController::getInstance();
$result = $obj1->certifyTicket($uuid, $zone, $price, $pubKey, $pubKey, $validTime);

header('Content-type: application/json charset=UTF-8');
echo $result;
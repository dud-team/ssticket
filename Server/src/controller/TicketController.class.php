<?php
require_once  $_SERVER['DOCUMENT_ROOT']."/PublicTicketSystemServer/src/model/DataBaseAccess.class.php";

class TicketController {
	
	static private $instance;
	private $passphrase = "1234";
		
	private function __construct()  {
		
	}
	
	public function __clone() {
		trigger_error('Clone is not allowed.', E_USER_ERROR);
	}
	
	static public function getInstance() {
		if (!isset(self::$instance)) {
			$c = __CLASS__;
			self::$instance = new $c;
		}
		
		return self::$instance;
	}
	
	private function fetchArray($mysqlObject) {
		$rows = array();
		while($r = $mysqlObject->fetch_array(MYSQL_ASSOC)) {
			$rows[] = $r;
		}
		return $rows;
	}
	 
	public function getTicketByID($t_id) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->listTicketByID($t_id);
		return $this->fetchArray($result);
	}
	
	public function removeTicket($t_id) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->deleteTicket($t_id);
		return $result;
	}
	
	public function createTicket($uuid, $zone, $price, $pubKey, $validTime) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->insertTicket($uuid, $zone, $price, $pubKey, $validTime);
		return $result;
		
	}
	
	public function updateTicket($t_id, $uuid, $zone, $price, $pubKey, $validTime) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->updateModule($t_id, $uuid, $zone, $price, $pubKey, $validTime);
		return $result;
	}

	public function certifyTicket($uuid, $zone, $price, $pubKey, $validTime) {
		
		$hash = sha1($uuid.$zone.$price.$pubKey.$validTime);
		$prk = file_get_contents('/var/www/key/private.pem');
		$pk  = openssl_get_privatekey(array($prk, $this->passphrase));
		openssl_private_encrypt($hash, $crypted, $pk);
		
		return base64_encode($crypted);
		
	}
	
}


?>
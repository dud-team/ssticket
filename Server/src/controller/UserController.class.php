<?php
require_once  $_SERVER['DOCUMENT_ROOT']."/PublicTicketSystemServer/src/model/DataBaseAccess.class.php";

class UserController {
	static private $instance;
	
	private function __construct()  {
	
	}
	
	public function __clone() {
		trigger_error('Clone is not allowed.', E_USER_ERROR);
	}
	
	static public function getInstance() {
		if (!isset(self::$instance)) {
			$c = __CLASS__;
			self::$instance = new $c;
		}
	
		return self::$instance;
	}
	
	private function fetchArray($mysqlObject) {
		$rows = array();
		while($r = $mysqlObject->fetch_array(MYSQL_ASSOC)) {
			$rows[] = $r;
		}
		return $rows;
	}
	
	public function checkLoginCredencials($login, $password) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->loginUser($login, $password);
		$count = $result->num_rows;
		if($count == 1) {
			return true;
		}
		return false;
	}
	
	public function getProfile($email) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->getProfile($email);
		return $this->fetchArray($result);
	}
	
	public function getUserFriendProfile($name) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->friend($name);
		return $this->fetchArray($result);
	}
	
	public function getDataByEmail($email) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->userDataByEmail($email);
		$count = $result->num_rows;
		if($count == 1) {
			return $this->fetchArray($result);
		}
		return false;
	}
	
	public function setRegisterUser($name, $lastName, $email, $password) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->registerUser($name, $lastName, $email, $password);
		return $result;
	}
	
	public function isUniqueEmail($email) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->listUserDetails($email);
		$count = $result->num_rows;
		if($count == 0) {
			return "true";
		}
		return "false";
	}
	
	public function editUser($name, $lastName, $email) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->updateUser($name, $lastName, $email);
		if($result) {
			session_start();
			$_SESSION['name'] = $name;
			$_SESSION['last_name'] = $lastName;
		}
		return $result;
	}
	
	public function deleteUser($email) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->removeUser($email);
		return $result;
	}

}






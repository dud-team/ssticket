-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u1
-- http://www.phpmyadmin.net
--
-- Máquina: localhost
-- Data de Criação: 15-Jul-2014 às 18:25
-- Versão do servidor: 5.5.37
-- versão do PHP: 5.4.4-14+deb7u12

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `publicticketsystem`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `ticket`
--

CREATE TABLE IF NOT EXISTS `ticket` (
  `t_id` int(8) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) NOT NULL,
  `zone` int(8) NOT NULL,
  `price` int(8) NOT NULL,
  `pubKey` text NOT NULL,
  `validTime` int(8) NOT NULL,
  PRIMARY KEY (`t_id`),
  KEY `zone` (`zone`),
  KEY `validTime` (`validTime`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `ticket`
--

INSERT INTO `ticket` (`t_id`, `uuid`, `zone`, `price`, `pubKey`, `validTime`) VALUES
(2, 'asdasd', 0, 6, 'sdfsdf', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ticket_type`
--

CREATE TABLE IF NOT EXISTS `ticket_type` (
  `id` int(8) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `ticket_type`
--

INSERT INTO `ticket_type` (`id`, `name`) VALUES
(0, 'Hour Ticket'),
(1, 'Day Ticket');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `u_id` int(8) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`u_id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `user`
--

INSERT INTO `user` (`u_id`, `email`, `name`, `last_name`, `password`) VALUES
(1, 'teste@teste.com', 'Teste', 'User', '2e6f9b0d5885b6010f9167787445617f553a735f');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user_ticket`
--

CREATE TABLE IF NOT EXISTS `user_ticket` (
  `t_id` int(8) NOT NULL,
  `u_id` int(8) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`t_id`,`u_id`),
  KEY `u_id` (`u_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `zone`
--

CREATE TABLE IF NOT EXISTS `zone` (
  `id` int(8) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `zone`
--

INSERT INTO `zone` (`id`, `name`) VALUES
(0, 'City Center'),
(1, 'Regional 1'),
(2, 'Regional 2');

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `user_ticket`
--
ALTER TABLE `user_ticket`
  ADD CONSTRAINT `user_ticket_ibfk_1` FOREIGN KEY (`t_id`) REFERENCES `ticket` (`t_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_ticket_ibfk_2` FOREIGN KEY (`u_id`) REFERENCES `user` (`u_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

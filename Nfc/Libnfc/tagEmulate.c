/*-
 * Free/Libre Near Field Communication (NFC) library
 *
 * Libnfc historical contributors:
 * Copyright (C) 2009      Roel Verdult
 * Copyright (C) 2009-2013 Romuald Conty
 * Copyright (C) 2010-2012 Romain Tartire
 * Copyright (C) 2010-2013 Philippe Teuwen
 * Copyright (C) 2012-2013 Ludovic Rousseau
 * See AUTHORS file for a more comprehensive list of contributors.
 * Additional contributors of this file:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  1) Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  2 )Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Note that this license only applies on the examples, NFC library itself is under LGPL
 *
 */

/**
 * File updated to run the SSTicket project
 */

/**
 * @file nfc-emulate-forum-tag4.c
 * @brief Emulates a NFC Forum Tag Type 4 v2.0 (or v1.0) with a NDEF message
 */

/*
 * This implementation was written based on information provided by the
 * following documents:
 *
 * NFC Forum Type 4 Tag Operation
 *  Technical Specification
 *  NFCForum-TS-Type-4-Tag_1.0 - 2007-03-13
 *  NFCForum-TS-Type-4-Tag_2.0 - 2010-11-18
 */

// Notes & differences with nfc-emulate-tag:
// - This example only works with PN532 because it relies on
//   its internal handling of ISO14443-4 specificities.
// - Thanks to this internal handling & injection of WTX frames,
//   this example works on readers very strict on timing
#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif // HAVE_CONFIG_H
#include <sys/types.h>
#include <sys/stat.h>
#include <assert.h>

#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <time.h>
#include <gmp.h>

#include <nfc/nfc.h>
#include <nfc/nfc-emulation.h>

#include "nfc-utils.h"

#include <openssl/sha.h>
#include <openssl/pem.h>
#include <openssl/ssl.h>
#include <openssl/rsa.h>
#include <openssl/evp.h>
#include <openssl/bio.h>
#include <openssl/err.h>

// Global Var
//static uint8_t *modulus;
static int random_number;
static mpz_t ms;
static mpz_t enc;
static uint8_t modulus[620];
static int accumulate = 0;
static int certAccumulate = 617;
static char *restEnc;
static int currentChunk = 0;
static int currentChunkCert = 0;
static uint8_t myModulusHash[41] = { 0x31, 0x63, 0x30, 0x37, 0x36, 0x30, 0x33,
		0x32, 0x39, 0x32, 0x35, 0x33, 0x36, 0x62, 0x34, 0x63, 0x38, 0x30, 0x64,
		0x62, 0x32, 0x66, 0x31, 0x62, 0x63, 0x35, 0x63, 0x36, 0x61, 0x38, 0x35,
		0x64, 0x66, 0x66, 0x38, 0x37, 0x33, 0x39, 0x37, 0x33, 0x62 };

static u_int8_t myModulus[617] = { 0x32, 0x34, 0x33, 0x36, 0x39, 0x38, 0x37,
		0x35, 0x36, 0x36, 0x32, 0x35, 0x38, 0x33, 0x36, 0x39, 0x38, 0x35, 0x35,
		0x30, 0x34, 0x32, 0x33, 0x39, 0x33, 0x38, 0x31, 0x31, 0x37, 0x33, 0x32,
		0x38, 0x32, 0x35, 0x33, 0x34, 0x39, 0x35, 0x34, 0x38, 0x33, 0x33, 0x36,
		0x35, 0x32, 0x35, 0x39, 0x39, 0x31, 0x35, 0x37, 0x37, 0x34, 0x39, 0x37,
		0x33, 0x39, 0x35, 0x38, 0x34, 0x38, 0x38, 0x30, 0x33, 0x33, 0x38, 0x39,
		0x30, 0x31, 0x34, 0x33, 0x34, 0x35, 0x30, 0x34, 0x35, 0x30, 0x36, 0x37,
		0x39, 0x38, 0x36, 0x38, 0x35, 0x34, 0x31, 0x34, 0x37, 0x31, 0x35, 0x33,
		0x39, 0x34, 0x38, 0x34, 0x31, 0x34, 0x37, 0x30, 0x32, 0x32, 0x34, 0x30,
		0x36, 0x33, 0x33, 0x38, 0x35, 0x34, 0x36, 0x39, 0x39, 0x35, 0x30, 0x36,
		0x32, 0x32, 0x38, 0x37, 0x33, 0x37, 0x33, 0x38, 0x30, 0x38, 0x34, 0x30,
		0x32, 0x34, 0x31, 0x35, 0x32, 0x37, 0x33, 0x30, 0x32, 0x35, 0x36, 0x39,
		0x34, 0x33, 0x32, 0x35, 0x35, 0x30, 0x37, 0x33, 0x38, 0x38, 0x32, 0x39,
		0x32, 0x33, 0x39, 0x36, 0x34, 0x33, 0x38, 0x33, 0x37, 0x37, 0x34, 0x35,
		0x33, 0x32, 0x39, 0x33, 0x35, 0x34, 0x35, 0x35, 0x33, 0x39, 0x34, 0x34,
		0x39, 0x36, 0x37, 0x36, 0x33, 0x34, 0x31, 0x30, 0x32, 0x35, 0x30, 0x38,
		0x30, 0x30, 0x39, 0x31, 0x31, 0x30, 0x36, 0x38, 0x39, 0x32, 0x39, 0x32,
		0x32, 0x34, 0x35, 0x36, 0x30, 0x34, 0x39, 0x39, 0x30, 0x34, 0x39, 0x34,
		0x39, 0x39, 0x37, 0x36, 0x39, 0x31, 0x32, 0x31, 0x35, 0x30, 0x36, 0x30,
		0x37, 0x31, 0x37, 0x31, 0x38, 0x32, 0x34, 0x37, 0x34, 0x32, 0x38, 0x39,
		0x33, 0x39, 0x36, 0x35, 0x31, 0x32, 0x37, 0x32, 0x30, 0x34, 0x37, 0x31,
		0x39, 0x37, 0x32, 0x39, 0x39, 0x38, 0x38, 0x37, 0x39, 0x35, 0x34, 0x36,
		0x32, 0x35, 0x39, 0x34, 0x31, 0x37, 0x31, 0x35, 0x33, 0x36, 0x30, 0x33,
		0x39, 0x32, 0x38, 0x32, 0x33, 0x38, 0x30, 0x33, 0x30, 0x32, 0x32, 0x37,
		0x36, 0x33, 0x33, 0x31, 0x36, 0x30, 0x39, 0x38, 0x36, 0x33, 0x30, 0x32,
		0x32, 0x32, 0x30, 0x36, 0x33, 0x37, 0x38, 0x31, 0x36, 0x35, 0x30, 0x34,
		0x34, 0x35, 0x33, 0x35, 0x39, 0x31, 0x31, 0x36, 0x35, 0x32, 0x32, 0x33,
		0x35, 0x35, 0x32, 0x36, 0x32, 0x30, 0x36, 0x39, 0x34, 0x36, 0x34, 0x34,
		0x32, 0x35, 0x36, 0x38, 0x32, 0x36, 0x32, 0x39, 0x30, 0x38, 0x30, 0x34,
		0x32, 0x37, 0x37, 0x35, 0x32, 0x32, 0x38, 0x33, 0x39, 0x32, 0x37, 0x37,
		0x36, 0x32, 0x37, 0x33, 0x39, 0x34, 0x35, 0x31, 0x36, 0x36, 0x30, 0x32,
		0x30, 0x37, 0x37, 0x39, 0x31, 0x39, 0x36, 0x39, 0x32, 0x32, 0x36, 0x38,
		0x32, 0x39, 0x37, 0x34, 0x32, 0x33, 0x39, 0x33, 0x37, 0x35, 0x36, 0x33,
		0x39, 0x37, 0x31, 0x34, 0x32, 0x30, 0x38, 0x38, 0x38, 0x39, 0x30, 0x33,
		0x39, 0x39, 0x35, 0x38, 0x31, 0x36, 0x37, 0x32, 0x32, 0x38, 0x39, 0x33,
		0x34, 0x39, 0x30, 0x33, 0x38, 0x37, 0x33, 0x37, 0x35, 0x30, 0x34, 0x32,
		0x33, 0x31, 0x34, 0x33, 0x34, 0x37, 0x36, 0x37, 0x33, 0x33, 0x37, 0x39,
		0x38, 0x35, 0x34, 0x30, 0x32, 0x38, 0x31, 0x33, 0x30, 0x30, 0x31, 0x34,
		0x39, 0x30, 0x39, 0x38, 0x33, 0x38, 0x30, 0x32, 0x31, 0x37, 0x31, 0x36,
		0x32, 0x33, 0x32, 0x31, 0x34, 0x31, 0x31, 0x30, 0x34, 0x30, 0x36, 0x35,
		0x39, 0x33, 0x35, 0x31, 0x38, 0x37, 0x37, 0x32, 0x30, 0x32, 0x35, 0x39,
		0x36, 0x32, 0x34, 0x31, 0x32, 0x33, 0x33, 0x31, 0x39, 0x33, 0x33, 0x33,
		0x32, 0x38, 0x32, 0x39, 0x38, 0x34, 0x38, 0x33, 0x35, 0x38, 0x31, 0x37,
		0x39, 0x30, 0x37, 0x33, 0x38, 0x30, 0x34, 0x31, 0x32, 0x38, 0x31, 0x33,
		0x33, 0x32, 0x35, 0x35, 0x38, 0x37, 0x30, 0x36, 0x34, 0x33, 0x32, 0x30,
		0x34, 0x38, 0x38, 0x33, 0x35, 0x33, 0x39, 0x31, 0x35, 0x37, 0x31, 0x31,
		0x38, 0x38, 0x34, 0x31, 0x34, 0x34, 0x36, 0x31, 0x32, 0x32, 0x32, 0x34,
		0x30, 0x33, 0x30, 0x37, 0x30, 0x37, 0x37, 0x35, 0x32, 0x38, 0x39, 0x37,
		0x30, 0x31, 0x34, 0x39, 0x30, 0x38, 0x35, 0x31, 0x35, 0x30, 0x36, 0x31,
		0x36, 0x33, 0x33, 0x30, 0x39, 0x32, 0x37, 0x36, 0x39, 0x36, 0x31, 0x35,
		0x32, 0x39, 0x31, 0x33, 0x39, 0x33, 0x38, 0x31, 0x37, 0x36, 0x34, 0x36,
		0x39, 0x39, 0x33, 0x30, 0x30, 0x34, 0x39, 0x32, 0x37, 0x33 };

static u_int8_t myPrivate[617] = { 0x31, 0x35, 0x30, 0x31, 0x31, 0x35, 0x34,
		0x38, 0x39, 0x30, 0x33, 0x36, 0x34, 0x39, 0x39, 0x30, 0x36, 0x33, 0x31,
		0x39, 0x37, 0x39, 0x38, 0x31, 0x39, 0x36, 0x31, 0x36, 0x30, 0x38, 0x39,
		0x34, 0x34, 0x38, 0x30, 0x38, 0x38, 0x32, 0x37, 0x34, 0x38, 0x31, 0x31,
		0x38, 0x37, 0x30, 0x34, 0x35, 0x39, 0x31, 0x32, 0x39, 0x37, 0x30, 0x36,
		0x34, 0x32, 0x33, 0x34, 0x30, 0x30, 0x37, 0x36, 0x38, 0x39, 0x32, 0x31,
		0x35, 0x39, 0x34, 0x30, 0x34, 0x37, 0x37, 0x38, 0x31, 0x37, 0x30, 0x31,
		0x38, 0x35, 0x37, 0x34, 0x31, 0x33, 0x33, 0x39, 0x38, 0x38, 0x35, 0x32,
		0x36, 0x33, 0x31, 0x32, 0x39, 0x39, 0x38, 0x39, 0x39, 0x37, 0x34, 0x32,
		0x39, 0x36, 0x30, 0x32, 0x39, 0x32, 0x38, 0x37, 0x36, 0x30, 0x37, 0x31,
		0x30, 0x32, 0x30, 0x32, 0x35, 0x30, 0x30, 0x35, 0x38, 0x32, 0x30, 0x34,
		0x30, 0x37, 0x36, 0x33, 0x33, 0x30, 0x32, 0x37, 0x34, 0x35, 0x37, 0x33,
		0x35, 0x36, 0x39, 0x30, 0x30, 0x37, 0x39, 0x33, 0x39, 0x34, 0x33, 0x31,
		0x37, 0x31, 0x36, 0x35, 0x30, 0x32, 0x34, 0x36, 0x30, 0x31, 0x33, 0x33,
		0x36, 0x34, 0x38, 0x38, 0x37, 0x30, 0x37, 0x36, 0x35, 0x32, 0x35, 0x38,
		0x36, 0x38, 0x33, 0x35, 0x34, 0x33, 0x34, 0x37, 0x34, 0x39, 0x35, 0x36,
		0x38, 0x36, 0x39, 0x30, 0x36, 0x33, 0x32, 0x35, 0x34, 0x37, 0x35, 0x32,
		0x37, 0x30, 0x33, 0x38, 0x39, 0x33, 0x35, 0x37, 0x38, 0x36, 0x38, 0x35,
		0x30, 0x36, 0x34, 0x32, 0x36, 0x35, 0x39, 0x30, 0x30, 0x39, 0x37, 0x31,
		0x39, 0x38, 0x37, 0x36, 0x32, 0x30, 0x30, 0x36, 0x39, 0x33, 0x38, 0x31,
		0x36, 0x31, 0x35, 0x35, 0x33, 0x33, 0x34, 0x39, 0x32, 0x39, 0x31, 0x39,
		0x33, 0x32, 0x31, 0x31, 0x34, 0x36, 0x32, 0x39, 0x35, 0x34, 0x38, 0x36,
		0x32, 0x35, 0x31, 0x39, 0x32, 0x38, 0x39, 0x39, 0x34, 0x31, 0x37, 0x31,
		0x30, 0x38, 0x37, 0x38, 0x32, 0x37, 0x37, 0x38, 0x30, 0x39, 0x35, 0x32,
		0x35, 0x38, 0x34, 0x38, 0x31, 0x30, 0x32, 0x34, 0x34, 0x38, 0x35, 0x34,
		0x33, 0x36, 0x36, 0x39, 0x35, 0x35, 0x32, 0x38, 0x37, 0x31, 0x32, 0x34,
		0x35, 0x37, 0x38, 0x39, 0x30, 0x31, 0x34, 0x31, 0x37, 0x31, 0x32, 0x30,
		0x30, 0x34, 0x34, 0x36, 0x38, 0x32, 0x36, 0x33, 0x33, 0x34, 0x35, 0x33,
		0x33, 0x35, 0x35, 0x32, 0x34, 0x34, 0x30, 0x38, 0x34, 0x30, 0x39, 0x39,
		0x35, 0x34, 0x35, 0x39, 0x32, 0x37, 0x36, 0x33, 0x35, 0x32, 0x35, 0x32,
		0x37, 0x38, 0x34, 0x36, 0x30, 0x39, 0x32, 0x33, 0x39, 0x33, 0x37, 0x33,
		0x30, 0x35, 0x32, 0x32, 0x34, 0x32, 0x32, 0x37, 0x39, 0x31, 0x37, 0x33,
		0x30, 0x35, 0x34, 0x34, 0x30, 0x39, 0x37, 0x33, 0x32, 0x35, 0x31, 0x34,
		0x34, 0x35, 0x36, 0x31, 0x39, 0x34, 0x37, 0x33, 0x39, 0x38, 0x35, 0x34,
		0x37, 0x38, 0x31, 0x36, 0x35, 0x31, 0x36, 0x32, 0x36, 0x36, 0x35, 0x32,
		0x35, 0x34, 0x35, 0x34, 0x32, 0x34, 0x38, 0x33, 0x31, 0x31, 0x32, 0x30,
		0x34, 0x34, 0x30, 0x36, 0x38, 0x30, 0x34, 0x33, 0x38, 0x38, 0x36, 0x33,
		0x34, 0x33, 0x37, 0x35, 0x32, 0x33, 0x36, 0x38, 0x31, 0x35, 0x37, 0x39,
		0x35, 0x36, 0x34, 0x35, 0x36, 0x32, 0x36, 0x32, 0x32, 0x30, 0x32, 0x39,
		0x30, 0x30, 0x35, 0x31, 0x36, 0x32, 0x36, 0x31, 0x34, 0x35, 0x38, 0x39,
		0x35, 0x38, 0x38, 0x39, 0x32, 0x38, 0x36, 0x30, 0x34, 0x35, 0x37, 0x30,
		0x37, 0x39, 0x30, 0x39, 0x31, 0x31, 0x38, 0x31, 0x34, 0x31, 0x37, 0x39,
		0x32, 0x37, 0x36, 0x33, 0x36, 0x36, 0x35, 0x32, 0x39, 0x37, 0x38, 0x39,
		0x37, 0x30, 0x37, 0x32, 0x31, 0x33, 0x31, 0x33, 0x34, 0x37, 0x39, 0x34,
		0x36, 0x37, 0x30, 0x36, 0x38, 0x30, 0x37, 0x35, 0x31, 0x36, 0x37, 0x35,
		0x36, 0x39, 0x35, 0x38, 0x38, 0x30, 0x35, 0x37, 0x37, 0x30, 0x30, 0x39,
		0x32, 0x31, 0x37, 0x37, 0x31, 0x39, 0x30, 0x31, 0x32, 0x38, 0x31, 0x30,
		0x38, 0x31, 0x35, 0x37, 0x37, 0x35, 0x39, 0x36, 0x37, 0x39, 0x34, 0x36,
		0x33, 0x35, 0x30, 0x33, 0x31, 0x30, 0x31, 0x31, 0x39, 0x31, 0x33, 0x37,
		0x36, 0x33, 0x30, 0x32, 0x39, 0x33, 0x38, 0x33, 0x36, 0x37, 0x35, 0x38,
		0x34, 0x39, 0x30, 0x31, 0x39, 0x33, 0x31, 0x32, 0x33, 0x35, 0x36, 0x36,
		0x33, 0x32, 0x30, 0x30, 0x37, 0x31, 0x36, 0x38, 0x31 };

char publicKey[] = "-----BEGIN PUBLIC KEY-----\n"
		"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAy8Dbv8prpJ/0kKhlGeJY\n"
		"ozo2t60EG8L0561g13R29LvMR5hyvGZlGJpmn65+A4xHXInJYiPuKzrKUnApeLZ+\n"
		"vw1HocOAZtWK0z3r26uA8kQYOKX9Qt/DbCdvsF9wF8gRK0ptx9M6R13NvBxvVQAp\n"
		"fc9jB9nTzphOgM4JiEYvlV8FLhg9yZovMYd6Wwf3aoXK891VQxTr/kQYoq1Yp+68\n"
		"i6T4nNq7NWC+UNVjQHxNQMQMzU6lWCX8zyg3yH88OAQkUXIXKfQ+NkvYQ1cxaMoV\n"
		"PpY72+eVthKzpMeyHkBn7ciumk5qgLTEJAfWZpe4f4eFZj/Rc8Y8Jj2IS5kVPjUy\n"
		"wQIDAQAB\n"
		"-----END PUBLIC KEY-----\n";

char privateKey[] = "-----BEGIN RSA PRIVATE KEY-----\n"
		"MIIEowIBAAKCAQEAy8Dbv8prpJ/0kKhlGeJYozo2t60EG8L0561g13R29LvMR5hy\n"
		"vGZlGJpmn65+A4xHXInJYiPuKzrKUnApeLZ+vw1HocOAZtWK0z3r26uA8kQYOKX9\n"
		"Qt/DbCdvsF9wF8gRK0ptx9M6R13NvBxvVQApfc9jB9nTzphOgM4JiEYvlV8FLhg9\n"
		"yZovMYd6Wwf3aoXK891VQxTr/kQYoq1Yp+68i6T4nNq7NWC+UNVjQHxNQMQMzU6l\n"
		"WCX8zyg3yH88OAQkUXIXKfQ+NkvYQ1cxaMoVPpY72+eVthKzpMeyHkBn7ciumk5q\n"
		"gLTEJAfWZpe4f4eFZj/Rc8Y8Jj2IS5kVPjUywQIDAQABAoIBADhg1u1Mv1hAAlX8\n"
		"omz1Gn2f4AAW2aos2cM5UDCNw1SYmj+9SRIkaxjRsE/C4o9sw1oxrg1/z6kajV0e\n"
		"N/t008FdlVKHXAIYWF93JMoVvIpMmT8jft6AN/y3NMpivgt2inmmEJZYNioFJKZG\n"
		"X+/vKYvsVISZm2fw8NfnKvAQK55yu+GRWBZGOeS9K+LbYvOwcrjKhHz66m4bedKd\n"
		"gVAix6NE5iwmjNXktSQlJMCjbtdNXg/xo1/G4kG2p/MO1HLcKfe1N5FgBiXj3Qjl\n"
		"vgvjJZkh1as2KTgaPOBqZaP03738VnYg23ISyvfT/teArVGtxrmFP7939EvJFKpF\n"
		"1wTxuDkCgYEA7t0DR37zt+dEJy+5vm7zSmN97VenwQJFWMiulkHGa0yU3lLasxxu\n"
		"m0oUtndIjenIvSx6t3Y+agK2F3EPbb0AZ5wZ1p1IXs4vktgeQwSSBdqcM8LZFDvZ\n"
		"uPboQnJoRdIkd62XnP5ekIEIBAfOp8v2wFpSfE7nNH2u4CpAXNSF9HsCgYEA2l8D\n"
		"JrDE5m9Kkn+J4l+AdGfeBL1igPF3DnuPoV67BpgiaAgI4h25UJzXiDKKoa706S0D\n"
		"4XB74zOLX11MaGPMIdhlG+SgeQfNoC5lE4ZWXNyESJH1SVgRGT9nBC2vtL6bxCVV\n"
		"WBkTeC5D6c/QXcai6yw6OYyNNdp0uznKURe1xvMCgYBVYYcEjWqMuAvyferFGV+5\n"
		"nWqr5gM+yJMFM2bEqupD/HHSLoeiMm2O8KIKvwSeRYzNohKTdZ7FwgZYxr8fGMoG\n"
		"PxQ1VK9DxCvZL4tRpVaU5Rmknud9hg9DQG6xIbgIDR+f79sb8QjYWmcFGc1SyWOA\n"
		"SkjlykZ2yt4xnqi3BfiD9QKBgGqLgRYXmXp1QoVIBRaWUi55nzHg1XbkWZqPXvz1\n"
		"I3uMLv1jLjJlHk3euKqTPmC05HoApKwSHeA0/gOBmg404xyAYJTDcCidTg6hlF96\n"
		"ZBja3xApZuxqM62F6dV4FQqzFX0WWhWp5n301N33r0qR6FumMKJzmVJ1TA8tmzEF\n"
		"yINRAoGBAJqioYs8rK6eXzA8ywYLjqTLu/yQSLBn/4ta36K8DyCoLNlNxSuox+A5\n"
		"w6z2vEfRVQDq4Hm4vBzjdi3QfYLNkTiTqLcvgWZ+eX44ogXtdTDO7c+GeMKWz4XX\n"
		"uJSUVL5+CVjKLjZEJ6Qc2WZLl94xSwL71E41H4YciVnSCQxVc4Jw\n"
		"-----END RSA PRIVATE KEY-----\n";

static unsigned char encrypted[4098] = { };
static unsigned char decrypted[4098] = { };
//

static nfc_device *pnd;
static nfc_context *context;
static bool quiet_output = false;
// Version of the emulated type4 tag:
static int type4v = 2;

#define SYMBOL_PARAM_fISO14443_4_PICC   0x20

typedef enum {
	NONE, CC_FILE, NDEF_FILE
} file;

struct nfcforum_tag4_ndef_data {
	uint8_t *ndef_file;
	size_t ndef_file_len;
};

struct nfcforum_tag4_state_machine_data {
	file current_file;
};

uint8_t nfcforum_capability_container[] = { 0x00, 0x0F, /* CCLEN 15 bytes */
0x20, /* Mapping version 2.0, use option -1 to force v1.0 */
0x00, 0x54, /* MLe Maximum R-ADPU data size */
// Notes:
//  - I (Romuald) don't know why Nokia 6212 Classic refuses the NDEF message if MLe is more than 0xFD (any suggests are welcome);
//  - ARYGON devices doesn't support extended frame sending, consequently these devices can't sent more than 0xFE bytes as APDU, so 0xFB APDU data bytes.
//  - I (Romuald) don't know why ARYGON device doesn't ACK when MLe > 0x54 (ARYGON frame length = 0xC2 (192 bytes))
		0x00, 0xFF, /* MLc Maximum C-ADPU data size */
		0x04, /* T field of the NDEF File-Control TLV */
		0x06, /* L field of the NDEF File-Control TLV */
		/* V field of the NDEF File-Control TLV */
		0xE1, 0x04, /* File identifier */
		0xFF, 0xFE, /* Maximum NDEF Size */
		0x00, /* NDEF file read access condition */
		0x00, /* NDEF file write access condition */
};

/* C-ADPU offsets */
#define CLA  0
#define INS  1
#define P1   2
#define P2   3
#define LC   4
#define DATA 5

#define ISO144434A_RATS 0xE0

int padding = RSA_PKCS1_PADDING;

RSA * createRSA(unsigned char * key, int public) {
	RSA *rsa = NULL;
	BIO *keybio;
	keybio = BIO_new_mem_buf(key, -1);
	if (keybio == NULL ) {
		printf("Failed to create key BIO");
		return 0;
	}
	if (public) {
		rsa = PEM_read_bio_RSA_PUBKEY(keybio, &rsa, NULL, NULL );
	} else {
		rsa = PEM_read_bio_RSAPrivateKey(keybio, &rsa, NULL, NULL );
	}
	if (rsa == NULL ) {
		printf("Failed to create RSA");
	}

	return rsa;
}

int public_encrypt(unsigned char * data, int data_len, unsigned char * key,
		unsigned char *encrypted) {
	RSA * rsa = createRSA(key, 1);
	int result = RSA_public_encrypt(data_len, data, encrypted, rsa, padding);
	return result;
}
int private_decrypt(unsigned char * enc_data, int data_len, unsigned char * key,
		unsigned char *decrypted) {
	RSA * rsa = createRSA(key, 0);
	int result = RSA_private_decrypt(data_len, enc_data, decrypted, rsa,
			padding);
	return result;
}

int private_encrypt(unsigned char * data, int data_len, unsigned char * key,
		unsigned char *encrypted) {
	RSA * rsa = createRSA(key, 0);
	int result = RSA_private_encrypt(data_len, data, encrypted, rsa, padding);
	return result;
}
int public_decrypt(unsigned char * enc_data, int data_len, unsigned char * key,
		unsigned char *decrypted) {
	RSA * rsa = createRSA(key, 1);
	int result = RSA_public_decrypt(data_len, enc_data, decrypted, rsa,
			padding);
	return result;
}

void printLastError(char *msg) {
	char * err = malloc(130);
	;
	ERR_load_crypto_strings();
	ERR_error_string(ERR_get_error(), err);
	printf("%s ERROR: %s\n", msg, err);
	free(err);
}

char* getZone(uint8_t *entry) {
	if (entry[0] == 0x30) {
		return "City Center";
	} else if (entry[0] == 0x31) {
		return "Regional 1";
	} else if (entry[0] == 0x32) {
		return "Regional 2";
	}
	return "Zone Unknown";
}

char* getTime(u_int8_t *entry) {
	if (entry[0] == 0x30) {
		return "Hour Ticket";
	} else if (entry[0] == 0x31) {
		return "Day Ticket";
	} else if (entry[0] == 0x32) {
		return "Weekly Ticket";
	} else if (entry[0] == 0x33) {
		return "Monthly Ticket";
	}
	return "Time Unknown";
}

void createNdefMsg(const uint8_t *data_in, int szBytes, uint8_t *bOuput) {

	uint8_t ndef_file_ck[0xfffe];
	ndef_file_ck[0] = (uint8_t) 0x00; //Fixed
	ndef_file_ck[1] = (uint8_t) szBytes + 7; //Size of Data + Header
	ndef_file_ck[2] = 0xd1; //Fixed
	ndef_file_ck[3] = 0x01; //Fixed
	ndef_file_ck[4] = szBytes + 3; //Size of Data
	ndef_file_ck[5] = 0x54; //Fixed
	ndef_file_ck[6] = 0x02; //Fixed
	ndef_file_ck[7] = 0x65; //e
	ndef_file_ck[8] = 0x6e; //n

	int i;
	for (i = 0; i < szBytes; i++) {
		ndef_file_ck[i + 9] = data_in[i];
	}

	memcpy(bOuput, ndef_file_ck, 0xfffe);
}

void extractPayload(const uint8_t *data_in, size_t szBytes, uint8_t *bOuput) {
	memcpy(bOuput, data_in + 15, szBytes - 15);
}

void extractHashCode(const uint8_t *data_in, size_t szBytes, uint8_t *bOuput) {
	print_hex(data_in + 50, 20);
}

void get_timestamp(char *buffer, size_t buffersize) {
	time_t rawtime;
	struct tm * timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
	strftime(buffer, buffersize, "%G%m%d%H%M%S", timeinfo);
}


int compareHash(char *myHash, uint8_t *otherHash) {
	int i;
	for (i = 0; i < SHA_DIGEST_LENGTH; i++) {
		if (myHash[i] != otherHash[i]) {
			return 1;
		}
	}
	return 0;
}

int hashequal(const unsigned char *sha1Bin, const char *hexstr) {
	unsigned long hexvar = strtoul(hexstr, NULL, 16);
	unsigned char theVarWithHexValues[] = { hexvar >> 16, hexvar >> 8, hexvar };
	return memcmp(sha1Bin + 17, theVarWithHexValues, 3) == 0;
}

static int nfcforum_tag4_io(struct nfc_emulator *emulator,
		const uint8_t *data_in, const size_t data_in_len, uint8_t *data_out,
		const size_t data_out_len) {
	int res = 0;
	uint8_t *old;

	struct nfcforum_tag4_ndef_data *ndef_data =
			(struct nfcforum_tag4_ndef_data *) (emulator->user_data);
	struct nfcforum_tag4_state_machine_data *state_machine_data =
			(struct nfcforum_tag4_state_machine_data *) (emulator->state_machine->data);

	if (data_in_len == 0) {
		// No input data, nothing to do
		return res;
	}

// Show transmitted command
	if (!quiet_output) {
		//printf("    In: ");
		//print_hex(data_in, data_in_len);
	}

	if (data_in_len >= 4) {
		if (data_in[CLA] != 0x00)
			return -ENOTSUP;

#define ISO7816_SELECT         0xA4
#define ISO7816_READ_BINARY    0xB0
#define ISO7816_UPDATE_BINARY  0xD6

		switch (data_in[INS]) {
		case ISO7816_SELECT:

			switch (data_in[P1]) {
			case 0x00: /* Select by ID */
				if ((data_in[P2] | 0x0C) != 0x0C)
					return -ENOTSUP;

				const uint8_t ndef_capability_container[] = { 0xE1, 0x03 };
				const uint8_t ndef_file[] = { 0xE1, 0x04 };
				if ((data_in[LC] == sizeof(ndef_capability_container))
						&& (0
								== memcmp(ndef_capability_container,
										data_in + DATA, data_in[LC]))) {
					memcpy(data_out, "\x90\x00", res = 2);
					state_machine_data->current_file = CC_FILE;
				} else if ((data_in[LC] == sizeof(ndef_file))
						&& (0 == memcmp(ndef_file, data_in + DATA, data_in[LC]))) {
					memcpy(data_out, "\x90\x00", res = 2);
					state_machine_data->current_file = NDEF_FILE;
				} else {
					memcpy(data_out, "\x6a\x00", res = 2);
					state_machine_data->current_file = NONE;
				}

				break;
			case 0x04: /* Select by name */
				if (data_in[P2] != 0x00)
					return -ENOTSUP;

				const uint8_t ndef_tag_application_name_v1[] = { 0xD2, 0x76,
						0x00, 0x00, 0x85, 0x01, 0x00 };
				const uint8_t ndef_tag_application_name_v2[] = { 0xD2, 0x76,
						0x00, 0x00, 0x85, 0x01, 0x01 };
				if ((type4v == 1)
						&& (data_in[LC] == sizeof(ndef_tag_application_name_v1))
						&& (0
								== memcmp(ndef_tag_application_name_v1,
										data_in + DATA, data_in[LC])))
					memcpy(data_out, "\x90\x00", res = 2);
				else if ((type4v == 2)
						&& (data_in[LC] == sizeof(ndef_tag_application_name_v2))
						&& (0
								== memcmp(ndef_tag_application_name_v2,
										data_in + DATA, data_in[LC])))
					memcpy(data_out, "\x90\x00", res = 2);
				else
					memcpy(data_out, "\x6a\x82", res = 2);

				break;
			default:
				return -ENOTSUP;
			}

			break;
		case ISO7816_READ_BINARY:
			if ((size_t) (data_in[LC] + 2) > data_out_len) {
				return -ENOSPC;
			}
			switch (state_machine_data->current_file) {
			case NONE:
				memcpy(data_out, "\x6a\x82", res = 2);
				break;
			case CC_FILE:
				memcpy(data_out,
						nfcforum_capability_container + (data_in[P1] << 8) + data_in[P2],
						data_in[LC]);
				memcpy(data_out + data_in[LC], "\x90\x00", 2);
				res = data_in[LC] + 2;
				break;
			case NDEF_FILE:
				memcpy(data_out,
						ndef_data->ndef_file + (data_in[P1] << 8) + data_in[P2],
						data_in[LC]);
				memcpy(data_out + data_in[LC], "\x90\x00", 2);
				res = data_in[LC] + 2;
				break;
			}
			break;

		case ISO7816_UPDATE_BINARY:
			/*memcpy(ndef_data->ndef_file + (data_in[P1] << 8) + data_in[P2],
			 data_in + DATA, data_in[LC]);*/
			if (data_in_len > 14) {

				if (data_in[14] == 0x42) {
					int compSize = 2;
					uint8_t *comp = malloc((sizeof(uint8_t *) * compSize));
					comp[0] = 0x6f; //o
					comp[1] = 0x6b; //k
					createNdefMsg(comp, compSize, ndef_data->ndef_file);
				} else if (data_in[14] == 0x58) { // GetTicket (X)
					int payLoadSize = data_in_len - 15;
					uint8_t *outputTicket = malloc(
							(sizeof(uint8_t *) * payLoadSize));
					extractPayload(data_in, data_in_len, outputTicket);

					int i;
					int lastFound = 0;
					int counter = 0;
					for (i = 0; i < payLoadSize; i++) {
						if (outputTicket[i] == 0x2a) {
							int size = i - lastFound;
							uint8_t *out = malloc(
									(sizeof(uint8_t *) * size + 1));
							memcpy(out, outputTicket + lastFound, size);
							out[size] = 0x00;
							lastFound = i + 1;
							if (counter == 0) {
								printf("ID: %s\n", out);
							} else if (counter == 1) {
								printf("UUID: %s\n", out);
							} else if (counter == 2) {
								char *zoneAsText = getZone(out);
								printf("ZONE: %s\n", zoneAsText);
							} else if (counter == 3) {
								printf("PRICE: %s\n", out);
							} else if (counter == 4) {
								char *timeAsText = getTime(out);
								printf("VALID TIME: %s\n", timeAsText);
							} else {
								printf("SIG: %s\n", out);
							}
							counter++;
							free((void *) out);
						}

					}

					free((void *) outputTicket);
				} else if (data_in[14] == 0x5a) { // getStamp (Z)
					int payLoadSize = data_in_len - 15;
					int timestampSize = 25;
					uint8_t *outputStamp = malloc(
							(sizeof(uint8_t *) * payLoadSize));
					extractPayload(data_in, data_in_len, outputStamp);

					int i;
					int lastFound = 0;
					int counter = 0;

					for (i = 0; i < payLoadSize; i++) {
						if (outputStamp[i] == 0x20) {
							int size = i - lastFound;
							uint8_t *out = malloc(
									(sizeof(uint8_t *) * size + 1));
							memcpy(out, outputStamp + lastFound, size);
							out[size] = 0x00;
							lastFound = i + 1;
							if (counter == 0) {
								printf("DATE: %s\n", out);
							} else if (counter == 1) {
								printf("TIME: %s\n", out);
							} else {
								printf("Unknown\n");
							}
							counter++;
							free((void *) out);
						}

					}
					free((void *) outputStamp);
				}
				//////////////////////// STAMP ////////////////////////////
				else if (data_in[14] == 0x4d) { // receive  modulus chunks (M)
					int payLoadSize = data_in_len - 15;
					uint8_t *preModulus = malloc(
							(sizeof(uint8_t *) * payLoadSize));

					extractPayload(data_in, data_in_len, preModulus);
					//print_hex(preModulus, data_in_len);
					memcpy(modulus + accumulate, preModulus, payLoadSize);
					accumulate = accumulate + payLoadSize;
					free((void *) preModulus);
					printf("Received Chunk modulus\n");
				}

				else if (data_in[14] == 0x4e) { // receive END modulus and nonce (N)
					printf("Received end modulus\n");
					mpz_t pu;
					mpz_t mo;

					uint8_t pub[8] = { 0x36, 0x35, 0x35, 0x33, 0x37 };
					srand(time(NULL ));
					random_number = rand();
					int payLoadSize = data_in_len - 15;
					uint8_t *nonce = malloc((sizeof(uint8_t *) * payLoadSize));
					extractPayload(data_in, data_in_len, nonce);
					char *nRand;
					asprintf(&nRand, "%s%s%d", nonce, "990", random_number);

					printf("Nonce Combined\n");

					mpz_init(ms);
					mpz_init(pu);
					mpz_init(mo);
					mpz_init(enc);

					mpz_set_str(pu, pub, 10);
					mpz_set_str(ms, nRand, 10);
					mpz_set_str(mo, modulus, 10);
					//gmp_printf("MS %Zd\n", ms);
					printf("Nonce encrypted\n");
					mpz_powm(enc, ms, pu, mo);
					restEnc = mpz_get_str(NULL, 10, enc);

					int count = 0;
					while (restEnc[count] != '\0') {
						count++;
					}
					accumulate = count;

					mpz_clear(pu);
					mpz_clear(mo);
					free((void *) nonce);

				} else if (data_in[14] == 0x46) { // split output msg (F)

					if (accumulate == 0) {
						uint8_t *msg = malloc((sizeof(uint8_t *) * 1));
						msg[0] = 0x7c;
						createNdefMsg(msg, 1, ndef_data->ndef_file);
						free((void *) msg);
						currentChunk = 0;
						printf("Sending end msg\n");
					} else {
						printf("Sending encrypted msg\n");
						int remain = MIN(accumulate, 240);
						accumulate = accumulate - remain;
						createNdefMsg(restEnc + currentChunk, remain,
								ndef_data->ndef_file);
						currentChunk = currentChunk + remain;

					}
				}

				else if (data_in[14] == 0x43) { // send Certificate(C)
					if (certAccumulate == 0) {
						uint8_t *msg = malloc((sizeof(uint8_t *) * 1));
						msg[0] = 0x7c;
						createNdefMsg(msg, 1, ndef_data->ndef_file);
						free((void *) msg);
						currentChunkCert = 0;
						certAccumulate = 617;
						printf("Sending END certificate\n");
					} else {
						printf("Sending certificate\n");
						int remain = MIN(certAccumulate, 240);
						certAccumulate = certAccumulate - remain;
						createNdefMsg(myModulus + currentChunkCert, remain,
								ndef_data->ndef_file);
						currentChunkCert = currentChunkCert + remain;
					}
				}

				else if (data_in[14] == 0x51) { // send Certificate(Q)
					createNdefMsg(myModulusHash, 41, ndef_data->ndef_file);
					printf("Sending certificate sig\n");
				}

				else if (data_in[14] == 0x41) { // check answer (A)

					int payloadSize = data_in_len - 15;
					uint8_t *response = malloc(
							(sizeof(uint8_t *) * payloadSize));
					uint8_t *comp = malloc((sizeof(uint8_t *) * 2));
					uint8_t *hashNonce = malloc(
							sizeof(uint8_t) * SHA_DIGEST_LENGTH);

					extractPayload(data_in, data_in_len, response);
					uint8_t *myNonce = malloc(sizeof(uint8_t) * 2);
					sprintf(myNonce, "%d", random_number);
					SHA1(myNonce, 10, hashNonce);
					int result = hashequal(hashNonce, response);
					printf("Checking Challenge result: ");

					if (result == 0) {

						comp[0] = 0x6f; //o
						comp[1] = 0x6b; //k
						printf("OK\n");

					} else {
						comp[0] = 0x6b; //K
						comp[1] = 0x6f; //o
						printf("Failed\n");
					}
					createNdefMsg(comp, 2, ndef_data->ndef_file);

					free((void *) comp);
					free((void *) hashNonce);
					free((void *) response);

				}

				else if (data_in[14] == 0x53) { // Stamp Case (S)

					int newSize = data_in_len - 15; // Payload Size
					int timestampSize = 25;
					int finalOffset = 13;
					int smid = 0x31;

					uint8_t *payLoad = malloc((sizeof(uint8_t) * newSize));
					uint8_t *hash = malloc(sizeof(uint8_t) * SHA_DIGEST_LENGTH);
					uint8_t *hashReceived = malloc(
							sizeof(uint8_t) * SHA_DIGEST_LENGTH);
					uint8_t *timestamp = malloc(
							sizeof(uint8_t) * timestampSize);
					uint8_t ndef_file[0xfffe];

					extractPayload(data_in, data_in_len, payLoad);
					//extractHashCode(payLoad, newSize, hashReceived);
					get_timestamp(timestamp, timestampSize);
					SHA1(payLoad, newSize, hash);

					ndef_file[0] = (uint8_t) 0x00; //Fixed
					ndef_file[1] = (uint8_t) timestampSize + SHA_DIGEST_LENGTH
							+ (finalOffset - 2); //Size of Data + Header
					ndef_file[2] = 0xd1; //Fixed
					ndef_file[3] = 0x01; //Fixed
					ndef_file[4] = timestampSize + SHA_DIGEST_LENGTH
							+ (finalOffset - 6); //Size of Data
					ndef_file[5] = 0x54; //Fixed
					ndef_file[6] = 0x02; //Fixed
					ndef_file[7] = 0x65; //e
					ndef_file[8] = 0x6e; //n

					int j = 9; // Iterate over ndef_file;
					int k; // Iterate over payLoad
					for (k = 0; k < SHA_DIGEST_LENGTH; k++) {
						ndef_file[j] = hash[k];
						j++;
					}

					ndef_file[j] = 0x7c;
					j++;

					for (k = 0; k < timestampSize; k++) {
						ndef_file[j] = timestamp[k];
						j++;
					}

					ndef_file[j] = 0x7c;
					j++;

					ndef_file[j] = smid;
					j++;

					ndef_file[timestampSize + SHA_DIGEST_LENGTH
							+ (finalOffset - 1)] = 0x7c; // Message terminate in my protocol
					memcpy(ndef_data->ndef_file, ndef_file, 0xfffe);

					//print_hex(ndef_data->ndef_file, timestampSize + SHA_DIGEST_LENGTH + finalOffset);

					free((void *) timestamp);
					free((void *) hash);
					free((void *) hashReceived);
					free((void *) payLoad);

					mpz_clear(ms);
					mpz_clear(enc);
					free((void *) restEnc);
					printf("Ticket Stamped\n DONE\n");

				} else if (data_in[14] == 0x57) {

					char plainText[2048 / 8] = "Hello this is Felipe";
					int encrypted_length = public_encrypt(plainText,
							strlen(plainText), publicKey, encrypted);
					if (encrypted_length == -1) {
						printLastError("Public Encrypt failed ");
						exit(0);
					}
					printf("Encrypted length =%d\n", encrypted_length);

					int decrypted_length = private_decrypt(encrypted,
							encrypted_length, privateKey, decrypted);
					if (decrypted_length == -1) {
						printLastError("Private Decrypt failed ");
						exit(0);
					}
					printf("Decrypted Text =%s\n", decrypted);
					printf("Decrypted Length =%d\n", decrypted_length);
					certAccumulate = decrypted_length;

				} else if (data_in[14] == 0x58) {
					if (certAccumulate == 0) {
						uint8_t *msg = malloc((sizeof(uint8_t *) * 1));
						msg[0] = 0x7c;
						createNdefMsg(msg, 1, ndef_data->ndef_file);
						free((void *) msg);
						printf("Sending END certificate\n");
					} else {
						printf("Sending certificate\n");
						int remain = MIN(certAccumulate, 240);
						certAccumulate = certAccumulate - remain;
						createNdefMsg(encrypted + currentChunkCert, remain,
								ndef_data->ndef_file);
						currentChunkCert = currentChunkCert + remain;
					}

				} else if (data_in[14] == 0x56) {
					printf("Abort\n");
					exit(1);

				} else {
					printf("nothing to do\n");
				}

			}

			if ((data_in[P1] << 8) + data_in[P2] == 0) {
				ndef_data->ndef_file_len = (ndef_data->ndef_file[0] << 8)
						+ ndef_data->ndef_file[1] + 2;
			}

			memcpy(data_out, "\x90\x00", res = 2);
			break;
		default:
			// Unknown
			if (!quiet_output) {
				printf("Unknown frame, emulated target abort.\n");
			}
			res = -ENOTSUP;
		}
	} else {
		res = -ENOTSUP;
	}

// Show transmitted command
	if (!quiet_output) {
		if (res < 0) {
			ERR("%s (%d)", strerror(-res), -res);
		} else {
			//printf("    Out: ");
			//print_hex(data_out, res);
		}
	}
	return res;
}

static void stop_emulation(int sig) {
	(void) sig;
	if (pnd != NULL ) {
		nfc_abort_command(pnd);
	} else {
		nfc_exit(context);
		exit(EXIT_FAILURE);
	}
}

static int ndef_message_load(char *filename,
		struct nfcforum_tag4_ndef_data *tag_data) {
	struct stat sb;
	if (stat(filename, &sb) < 0) {
		printf("file not found or not accessible '%s'", filename);
		return -1;
	}

	/* Check file size */
	if (sb.st_size > 0xFFFF) {
		printf("file size too large '%s'", filename);
		return -1;
	}

	tag_data->ndef_file_len = sb.st_size + 2;

	tag_data->ndef_file[0] = (uint8_t) (sb.st_size >> 8);
	tag_data->ndef_file[1] = (uint8_t) (sb.st_size);

	FILE *F;
	if (!(F = fopen(filename, "r"))) {
		printf("fopen (%s, \"r\")", filename);
		return -1;
	}

	if (1 != fread(tag_data->ndef_file + 2, sb.st_size, 1, F)) {
		printf("Can't read from %s", filename);
		fclose(F);
		return -1;
	}

	fclose(F);
	return sb.st_size;
}

static int ndef_message_save(char *filename,
		struct nfcforum_tag4_ndef_data *tag_data) {
	FILE *F;
	if (!(F = fopen(filename, "w"))) {
		printf("fopen (%s, w)", filename);
		return -1;
	}

	if (1
			!= fwrite(tag_data->ndef_file + 2, tag_data->ndef_file_len - 2, 1,
					F)) {
		printf("fwrite (%d)", (int) tag_data->ndef_file_len - 2);
		fclose(F);
		return -1;
	}

	fclose(F);
	return tag_data->ndef_file_len - 2;
}

static void usage(char *progname) {
	fprintf(stderr, "usage: %s [-1] [infile [outfile]]\n", progname);
	fprintf(stderr, "      -1: force Tag Type 4 v1.0 (default is v2.0)\n");
}

static int keepRunning = 1;
static void intHandler(int sig) {
	keepRunning = 0;
}

int main(int argc, char *argv[]) {

	int options = 0;
	nfc_target nt = { .nm = { .nmt = NMT_ISO14443A, .nbr = NBR_UNDEFINED, // Will be updated by nfc_target_init()
			}, .nti = { .nai = { .abtAtqa = { 0x00, 0x04 }, .abtUid = { 0x08,
			0x00, 0xb0, 0x0b }, .szUidLen = 4, .btSak = 0x20, .abtAts = { 0x75,
			0x33, 0x92, 0x03 }, /* Not used by PN532 */
	.szAtsLen = 4, }, }, };

	uint8_t ndef_file[0xfffe] = { 0x00, 37, 0xd1, 0x01, 0x21, 0x54, 0x02, 0x65,
			0x6e, 0x31, 0x34, 0x36, 0x31, 0x67, 0x73, 0x61, 0x45, 0x6d, 0x74,
			0x7c, 0x31, 0x30, 0x7c, 0x30, 0x32, 0x31, 0x30, 0x7c, 0x7c, 0x32,
			0x7c, 0x31, 0x35, 0x30, 0x7c, 0x73, 0x69, 0x67, 0x7c };

	struct nfcforum_tag4_ndef_data nfcforum_tag4_data = {
			.ndef_file = ndef_file, .ndef_file_len = ndef_file[1] + 2, };

	struct nfcforum_tag4_state_machine_data state_machine_data = {
			.current_file = NONE, };

	struct nfc_emulation_state_machine state_machine = { .io = nfcforum_tag4_io,
			.data = &state_machine_data, };

	struct nfc_emulator emulator = { .target = &nt, .state_machine =
			&state_machine, .user_data = &nfcforum_tag4_data, };

	if ((argc > (1 + options)) && (0 == strcmp("-h", argv[1 + options]))) {
		usage(argv[0]);
		exit(EXIT_SUCCESS);
	}

	if ((argc > (1 + options)) && (0 == strcmp("-1", argv[1 + options]))) {
		type4v = 1;
		nfcforum_capability_container[2] = 0x10;
		options += 1;
	}

	if (argc > (3 + options)) {
		usage(argv[0]);
		exit(EXIT_FAILURE);
	}

// If some file is provided load it
	if (argc >= (2 + options)) {
		if (ndef_message_load(argv[1 + options], &nfcforum_tag4_data) < 0) {
			printf("Can't load NDEF file '%s'", argv[1 + options]);
			exit(EXIT_FAILURE);
		}
	}

	nfc_init(&context);
	if (context == NULL ) {
		ERR("Unable to init libnfc (malloc)\n");
		exit(EXIT_FAILURE);
	}

// Try to open the NFC reader
	pnd = nfc_open(context, NULL );

	if (pnd == NULL ) {
		ERR("Unable to open NFC device");
		nfc_exit(context);
		exit(EXIT_FAILURE);
	}

	signal(SIGINT, stop_emulation);
	signal(SIGINT, intHandler);
	do {
		printf("NFC device: %s opened\n", nfc_device_get_name(pnd));
		printf(
				"Emulating NDEF tag now, please touch it with a second NFC device\n");
		if (0 != nfc_emulate_target(pnd, &emulator, 0)) { // contains already nfc_target_init() call
		//nfc_perror(pnd, "nfc_emulate_target");
		//nfc_close(pnd);
		//nfc_exit(context);
			printf("EXIT 1\n");
			//exit(EXIT_FAILURE);
		}

		if (argc == (3 + options)) {
			if (ndef_message_save(argv[2 + options], &nfcforum_tag4_data) < 0) {
				printf("Can't save NDEF file '%s'", argv[2 + options]);
				//nfc_close(pnd);
				//nfc_exit(context);
				printf("EXIT 2\n");
				//exit(EXIT_FAILURE);
			}
		}
		//nfc_close(pnd);
		//nfc_exit(context);
		printf("FINAL\n");
		//exit(EXIT_SUCCESS);
	} while (keepRunning);

	nfc_close(pnd);
	nfc_exit(context);
	exit(EXIT_SUCCESS);

}

/*int
 main(int argc, char *argv[])
 {
 signal(SIGINT, intHandler);

 while(keepRunning) {
 auxmain(argc, argv);
 }
 return 0;

 }*/

package de.tud.inf.publicticketsystem.security;

import java.math.BigInteger;
import java.security.SecureRandom;

public class RSA {
	private final static BigInteger one = new BigInteger("1");
	private final static SecureRandom random = new SecureRandom();
	public final static int DEFAULTLENGTH = 2048;
	
	public static BigInteger[] genKeys(int keylength) {

		int N = keylength;

		BigInteger p = BigInteger.probablePrime(N / 2, random);
		BigInteger q = BigInteger.probablePrime(N / 2, random);
		BigInteger phi = (p.subtract(one)).multiply(q.subtract(one));

		BigInteger modulus = p.multiply(q);
		BigInteger publicKey = new BigInteger("65537");
		BigInteger privateKey = publicKey.modInverse(phi);

		BigInteger[] bArgs = { modulus, publicKey, privateKey };

		return bArgs;
	}
	
	public static BigInteger encrypt(BigInteger message, BigInteger key, BigInteger modulus) {
		return message.modPow(key, modulus);
	}

	public static BigInteger decrypt(BigInteger encrypted, BigInteger key, BigInteger modulus) {
		BigInteger bResult = encrypted.modPow(key, modulus);
		return bResult;
	}
}

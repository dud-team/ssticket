package de.tud.inf.publicticketsystem.activity;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Comparator;
import java.util.concurrent.TimeUnit;

import de.tud.inf.publicticketsystem.R;
import de.tud.inf.publicticketsystem.controller.TicketController;
import de.tud.inf.publicticketsystem.db.DBBaseFacade;
import de.tud.inf.publicticketsystem.model.RoundedTransformation;
import de.tud.inf.publicticketsystem.model.Stamp;
import it.gmariotti.cardslib.library.internal.Card;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 10/8/14.
 */
public class TicketCard extends Card {

    private DBBaseFacade dbSqlite;
    private String id;
    private int zone;
    private int validTime;
    private TicketController ticketInstance = TicketController.getInstance();

    /**
     * Constructor with a custom inner layout
     *
     * @param context
     */
    public TicketCard(Context context, DBBaseFacade dbSqlite, String id, int zone, int validTime) {
        this(context, R.layout.ticket_item);
        this.id = id;
        this.zone = zone;
        this.validTime = validTime;
        this.dbSqlite = dbSqlite;

    }

    /**
     * @param context
     * @param innerLayout
     */
    public TicketCard(Context context, int innerLayout) {
        super(context, innerLayout);
        init();
    }

    /**
     * Init
     */
    private void init() {

        //No Header


        //Set a OnClickListener listener
        setOnClickListener(new OnCardClickListener() {
            @Override
            public void onClick(Card card, View view) {
                Intent myIntent = new Intent(getContext(), TicketDetails.class);
                myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                myIntent.putExtra("ticket_id", Integer.valueOf(getId()));
                getContext().getApplicationContext().startActivity(myIntent);
            }
        });

    }


    @Override
    public OnUndoHideSwipeListListener getOnUndoHideSwipeListListener() {
        dbSqlite.deleteTicket(Integer.parseInt(getId()));
        return super.getOnUndoHideSwipeListListener();

    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {

        TextView tvTicketID = (TextView) parent
                .findViewById(R.id.tvTicketName);
        TextView tvTicketTime = (TextView) parent.findViewById(R.id.tvTicketTime);
        TextView tvTicketRegion = (TextView) parent.findViewById(R.id.tvTicketRegion);

        ImageView ivExpired = (ImageView) parent.findViewById(R.id.ivExpired);

        Stamp dbstamp = dbSqlite.retriveStampById(Integer.parseInt(getId()));
        String stamp = ticketInstance.getStampTimeOnDB(dbstamp);
        tvTicketRegion.setText(ticketInstance.convertTicketZone(getZone()));


        boolean stampValid = ticketInstance.hasValidStamp(stamp);
        String timeRem;
        if (stampValid) {
            timeRem = ticketInstance.getTicketRemainingTime(getValidTime(), dbstamp);
            if(!timeRem.equals("Expired")) {
                long millis = ticketInstance.getTicketRemainingTimeInLong(getValidTime(),
                        dbSqlite.retriveStampById(Integer.valueOf(getId())));
                StringBuilder sb = new StringBuilder();
                long days = TimeUnit.MILLISECONDS.toDays(millis);
                if (days != 0) {
                    sb.append(days);
                    if (days >= 2) {
                        sb.append(" days ");
                    } else {
                        sb.append(" day ");
                    }
                } else {
                    sb.append(String.format("%02d:%02d:%02d",

                            TimeUnit.MILLISECONDS.toHours(millis) -
                                    TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis)),
                            TimeUnit.MILLISECONDS.toMinutes(millis) -
                                    TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), // The change is in this line
                            TimeUnit.MILLISECONDS.toSeconds(millis) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))));
                }
                tvTicketID.setText(sb.toString());
            }else {
                tvTicketID.setText(timeRem);
            }
        } else {
            timeRem = "Undefined";
            tvTicketID.setText(timeRem);
        }

        tvTicketTime.setText(ticketInstance.convertTicketTime(getValidTime()));
        if (timeRem.equals("Expired")) {
            Picasso.with(getContext()).load(R.drawable.red).resize(144, 144).transform(
                    new RoundedTransformation(100, 0)).into(ivExpired);
        } else if (stampValid) {
            Picasso.with(getContext()).load(R.drawable.yellow).resize(144, 144).transform(
                    new RoundedTransformation(100, 0)).into(ivExpired);
        } else {
            Picasso.with(getContext()).load(R.drawable.green).resize(144, 144).transform(
                    new RoundedTransformation(100, 0)).into(ivExpired);
        }

    }

    public String getId() {
        return id;
    }

    public void setId(String ticketId) {
        this.id = ticketId;
    }


    public int getValidTime() {
        return validTime;
    }

    public int getZone() {
        return zone;
    }

}
package de.tud.inf.publicticketsystem.security;

import java.math.BigInteger;

public class ECC {
	
	public static String DEFAULTLENGTH = "160";

	public ECC(String length) {
		// TODO Auto-generated constructor stub
	}
	
	public BigInteger[] genKeys() {
		//TODO implement real genKey
		BigInteger[] bArgs = { new BigInteger("1"), new BigInteger("2"), new BigInteger("3") };
		return bArgs;
	}

	public BigInteger encrypt(BigInteger message, BigInteger key,
			BigInteger modulus) {
		//TODO implement real Encrypt
		return new BigInteger("1");
	}

	public BigInteger decrypt(BigInteger encrypted, BigInteger key,
			BigInteger modulus) {
		// TODO implement real decrypt
		return new BigInteger("2");
	}

	public String getDefautKeyLength() {
		return ECC.DEFAULTLENGTH;
	}

	
/*	public static BigInteger[] genKeys() {
	
		KeyPairGenerator kpg;
		kpg = KeyPairGenerator.getInstance("EC", "SunEC");
		ECGenParameterSpec ecsp;
		ecsp = new ECGenParameterSpec("secp192r1");
		kpg.initialize(ecsp);
		KeyPair kp = kpg.genKeyPair();
		PrivateKey privKey = kp.getPrivate();
		PublicKey pubKey = kp.getPublic();
		System.out.println(privKey.toString());
		System.out.println(pubKey.toString());
	}*/
}
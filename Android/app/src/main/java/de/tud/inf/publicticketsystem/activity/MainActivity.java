package de.tud.inf.publicticketsystem.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.readystatesoftware.systembartint.SystemBarTintManager;

import org.spongycastle.util.encoders.Base64;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

import de.tud.inf.publicticketsystem.R;
import de.tud.inf.publicticketsystem.controller.TicketController;
import de.tud.inf.publicticketsystem.controller.UserController;
import de.tud.inf.publicticketsystem.db.DBBaseFacade;
import de.tud.inf.publicticketsystem.model.Ticket;
import de.tud.inf.publicticketsystem.security.SecureStorage;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardArrayAdapter;
import it.gmariotti.cardslib.library.view.CardListView;

public class MainActivity extends Activity {

    private DBBaseFacade dbSqlite;
    private CardArrayAdapter adapter;
    private ArrayList<Ticket> ltTicket;
    private SecureStorage mStorage = SecureStorage.getInstace();
    private UserController userInstance = UserController.getInstance();
    private SecureStorage dbFlash = SecureStorage.getInstace();
    private Spinner spZones;
    private Spinner spTicket;
    private TextView price;
    private TicketController ticketInstance = TicketController.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SystemBarTintManager tintManager = new SystemBarTintManager(this);
        tintManager.setStatusBarTintEnabled(true);
        tintManager.setStatusBarTintColor(getResources().getColor(R.color.blue_light));

        checkKeys();

        CardListView lvTicket = (CardListView) findViewById(R.id.ticketsListView);

        dbSqlite = new DBBaseFacade(getApplicationContext(), dbFlash.retriveConfig(getApplicationContext(), SecureStorage.USERREMEMBER));
        ltTicket = dbSqlite.retriveAllTicket();

        ArrayList<Card> cards = fromTicketToCard(ltTicket);
        adapter = new CardArrayAdapter(this,
                cards);
        adapter.setEnableUndo(true);
        lvTicket.setAdapter(adapter);
        lvTicket.setClipToPadding(false);

    }


    @Override
    protected void onResume() {
        super.onResume();
        updateListView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_buy:
                showGenerateTicket();
                return true;
            case R.id.action_settings:
                Intent myIntent = new Intent(MainActivity.this,
                        SettingActivity.class);
                MainActivity.this.startActivity(myIntent);
                return true;
            case R.id.action_logout:
                logoutApp();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showGenerateTicket() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final AlertDialog alert = builder.create();
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.activity_generate_ticket, null);
        View dialogTitle = inflater.inflate(R.layout.custon_dialog_title, null);

        spZones = (Spinner) dialoglayout.findViewById(R.id.spZone);
        spTicket = (Spinner) dialoglayout.findViewById(R.id.spTicketType);
        price = (TextView) dialoglayout.findViewById(R.id.tvPriceValue);
        Button confirm = (Button) dialoglayout.findViewById(R.id.btnConfirm);

        List<String> listZones = loadZones();
        List<String> listTickets = loadTickets();

        ArrayAdapter<String> dataAdapterZones = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, listZones);
        dataAdapterZones
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spZones.setAdapter(dataAdapterZones);

        ArrayAdapter<String> dataAdapterTicket = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, listTickets);
        dataAdapterTicket
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTicket.setAdapter(dataAdapterTicket);

        spZones.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // Do nothing
            }

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                updatePriceTextView();

            }

        });

        spTicket.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // Do nothing
            }

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                updatePriceTextView();

            }

        });

        confirm.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                long before = System.currentTimeMillis();
                if (storeTicket()) {
                    long after = System.currentTimeMillis();
                    long result = after - before;
                    Toast.makeText(getApplicationContext(), String.valueOf(result), Toast.LENGTH_LONG).show();
                    updateListView();
                } else {
                    Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                }
                alert.dismiss();
            }
        });

        alert.setView(dialoglayout);
        alert.setCustomTitle(dialogTitle);
        alert.show();
    }

    private void updateListView() {
        ltTicket = dbSqlite.retriveAllTicket();
        adapter.clear();
        adapter.addAll(fromTicketToCard(ltTicket));
    }

    private void checkKeys() {
        String myKeys = mStorage.retriveConfig(getApplicationContext(),
                SecureStorage.GENKEY);
        if (myKeys.equals("")) {
            Toast.makeText(getApplicationContext(), "Generating Keys...",
                    Toast.LENGTH_LONG).show();
            userInstance.genKeys(getApplicationContext());
        }
    }


    private void logoutApp() {
        mStorage.saveConfig(getApplicationContext(), "",
                SecureStorage.USERCONF);
        mStorage.saveConfig(getApplicationContext(), "",
                SecureStorage.USERREMEMBER);
        Intent myIntent = new Intent(MainActivity.this, LoginActivity.class);
        MainActivity.this.startActivity(myIntent);
        finish();
    }

    private ArrayList<Card> fromTicketToCard(ArrayList<Ticket> tickets) {
        ArrayList<Card> cards = new ArrayList<Card>();
        Collections.sort(tickets, new Comparator<Ticket>() {
            @Override
            public int compare(Ticket lhs, Ticket rhs) {
                return rhs.getId() - lhs.getId();
            }
        });
        for (int i = 0; i < tickets.size(); i++) {
            Ticket t = tickets.get(i);
            TicketCard card = new TicketCard(getApplicationContext(), dbSqlite, String.valueOf(t.getId()), t.getZone(), t.getValidTime());
            card.setShadow(true);
            card.setClickable(true);
            card.setSwipeable(true);
            cards.add(card);
        }
        return cards;
    }

    private boolean storeTicket() {
        String uuid = genUuidTicket();
        int zone = spZones.getLastVisiblePosition();
        String pubKey = getUserPublicKey();
        int validTime = spTicket.getLastVisiblePosition();
        String price = calculateTicketPrice();
        Ticket myTicket = new Ticket(1, uuid, zone, price, pubKey, validTime);
        return dbSqlite.insertIntoTicket(genUuidTicket(), myTicket.getZone(),
                myTicket.getPrice(), myTicket.getPubKey(),
                myTicket.getValidTime(), new String(Base64.encode(ticketInstance.signTicket(myTicket).getBytes())));

    }

    private String getUserPublicKey() {
        return new String(dbFlash.retriveConfig(getApplicationContext(), SecureStorage.MODULUS));
    }

    private String genUuidTicket() {
        return UUID.randomUUID().toString();
    }

    private List<String> loadTickets() {
        List<String> listTickets = new ArrayList<String>();
        listTickets.add("Hour Ticket");
        listTickets.add("Day Ticket");
        listTickets.add("Weekly Ticket");
        listTickets.add("Monthly Ticket");
        return listTickets;
    }

    private List<String> loadZones() {
        List<String> listZones = new ArrayList<String>();
        listZones.add("City Center");
        listZones.add("Regional 1");
        listZones.add("Regional 2");
        return listZones;
    }

    private String calculateTicketPrice() {
        int ticket = spTicket.getLastVisiblePosition();
        int zones = spZones.getLastVisiblePosition();
        double value = 0.0;

        switch (ticket) {
            case 0:
                value = 2 * (zones + 1);
                break;
            case 1:
                value = 5 * (zones + 1);
                break;
            case 2:
                value = 7 * (zones + 1);
                break;
            case 3:
                value = 9 * (zones + 1);
                break;
        }
        return String.valueOf(value);
    }

    private void updatePriceTextView() {
        price.setText(String.valueOf(calculateTicketPrice()));
    }

}

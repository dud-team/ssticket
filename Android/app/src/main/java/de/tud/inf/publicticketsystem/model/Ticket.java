package de.tud.inf.publicticketsystem.model;

import java.math.BigInteger;

public class Ticket {
	
	private int id;
	private String uuid;
	private int zone;
	private String price;
	private String pubKey;
	private int validTime;
	private String sig;
	
	public Ticket(int id, String uuid, int zone, String price, String pubKey, int validTime) {
		this.setId(id);
		this.setUuid(uuid);
		this.setPrice(price);
		this.setZone(zone);
		this.setPubKey(pubKey);
		this.setValidTime(validTime);
		this.sig = "";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public int getZone() {
		return zone;
	}

	public void setZone(int zone) {
		this.zone = zone;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getPubKey() {
		return pubKey;
	}
	
	public byte[] getPubKeyAsBytes() {
		BigInteger n = new BigInteger(pubKey);
		return n.toByteArray();
	}
	

	public void setPubKey(String pubKey) {
		this.pubKey = pubKey;
	}

	public int getValidTime() {
		return validTime;
	}

	public void setValidTime(int validTime) {
		this.validTime = validTime;
	}
	
	public void setSig(String sig) {
		this.sig = sig;
		
	}
	
	public String getSig() {
		return this.sig;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getId()).append("*");
		sb.append(getUuid()).append("*");
		sb.append(getZone()).append("*");
		sb.append(getPrice()).append("*");
		sb.append(getValidTime()).append("*");
		sb.append(getSig()).append("*");
		
		return sb.toString();
	}

}

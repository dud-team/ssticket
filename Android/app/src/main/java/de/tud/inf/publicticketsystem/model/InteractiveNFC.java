package de.tud.inf.publicticketsystem.model;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.TagLostException;
import android.nfc.tech.Ndef;
import de.tud.inf.publicticketsystem.exceptions.ReadOnlyTagException;
import de.tud.inf.publicticketsystem.exceptions.SpaceFullTagException;

public class InteractiveNFC {

	private Ndef tag;
	private boolean delay;

	public int TIMEDELAY = 100;

	public InteractiveNFC(Ndef tag) {
		this.setTag(tag);
		this.setDelay(false);
	}

	public boolean writeTag(NdefMessage msg) throws TagLostException, IOException,
			FormatException, InterruptedException, ReadOnlyTagException,
			SpaceFullTagException {

		// Make sure the tag is writable
		if (!tag.isWritable()) {
			throw new ReadOnlyTagException();
		}

		// Check if there's enough space on the tag for the message
		int size = msg.toByteArray().length;
		if (size > tag.getMaxSize()) {
			throw new SpaceFullTagException();
		}

		tag.writeNdefMessage(msg);
		if (this.delay) {
			Thread.sleep(TIMEDELAY);
		}

		return true;
	}

	public String readTag() throws IOException, FormatException {

		StringBuilder tagInfo = new StringBuilder("");
		NdefRecord[] records = tag.getNdefMessage().getRecords();

		if (records != null) {
			for (int l = 0; l < records.length; ++l) {
				if ((records[l].getTnf() == NdefRecord.TNF_WELL_KNOWN)
						&& Arrays.equals(records[l].getType(),
								NdefRecord.RTD_TEXT)) {
					byte[] payload = records[l].getPayload();
					String read = new String(Arrays.copyOfRange(payload, 1,
							payload.length), Charset.forName("UTF-8"));
					tagInfo.append(read);
				}
			}
		}
		
		
		return tagInfo.toString();
	}
	
	public void openConnection() throws IOException {
		tag.connect();
	}
	
	public void closeConnection() throws IOException {
		tag.close();
	}

	public Ndef getTag() {
		return tag;
	}

	public void setTag(Ndef tag) {
		this.tag = tag;
	}

	public boolean isDelay() {
		return delay;
	}

	public void setDelay(boolean delay) {
		this.delay = delay;
	}
	
	public int getTimeDelay() {
		return this.TIMEDELAY;
	}
	
	public void setTimeDelay(int timeDelay) {
		this.TIMEDELAY = timeDelay;
	}

}

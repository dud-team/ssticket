package de.tud.inf.publicticketsystem.db;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBAdapter {

	private static final String TAB_TICKET = "tab_ticket";
	private static final String TAB_STAMP = "tab_stamp";

	private DatabaseHelper mDbHelper;
	private SQLiteDatabase mDb;
	private Context mCtx;
	private String user;

	private static final String DB_NAME = "iticket_DB";
	private static final int DATABASE_VERSION = 12;

	private final static String CREATETICKET = "CREATE TABLE "
			+ TAB_TICKET
			+ "(t_id INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ "uuid varchar(40) NOT NULL, zone INTEGER NOT NULL, price VARCHAR(4) NOT NULL, pubkey TEXT, " 
			+ "validtime INTEGER NOT NULL, sig TEXT NOT NULL) ";
	
	private final static String CREATESTAMP = "CREATE TABLE "
			+ TAB_STAMP
			+ " (s_id INTEGER PRIMARY KEY, "
			+ " sig VARCHAR(255) NOT NULL, "
			+ " smid INTEGER NOT NULL, "
			+ " timestamp DATETIME, FOREIGN KEY(s_id) REFERENCES " + TAB_TICKET + "(t_id) ON DELETE CASCADE)";

	public DBAdapter(Context ctx, String user) {
		this.mCtx = ctx;
		this.user = user;
	}

	public DBAdapter open() throws SQLException {
		mDbHelper = new DatabaseHelper(mCtx, user);
		mDb = mDbHelper.getWritableDatabase();
		return this;
	}

	public void close() {
		mDbHelper.close();
		mDb.close();
	}

	public boolean insertIntoTicket(String uuid, int zone, String price,
			String pubKey, int validTime, String sig) {
		ContentValues values = new ContentValues();
		values.put("uuid", uuid);
		values.put("zone", zone);
		values.put("price", price);
		values.put("pubkey", pubKey);
		values.put("validtime", validTime);
		values.put("sig", sig);
		long resp = mDb.insert(TAB_TICKET, null, values);
		values.clear();

		return resp > -1;

	}

	public void deleteTicket(int tId) {

		mDb.delete(TAB_TICKET, "t_id=" + tId, null);
	}

	public Cursor retriveAllTickets() throws SQLException {

		return mDb.query(true, TAB_TICKET, new String[] { "t_id", "uuid",
				"zone", "price", "pubkey", "validtime", "sig" }, null, null, null,
				null, null, null);

	}

	public Cursor retriveTicketById(int tId) throws SQLException {

		return mDb.query(true, TAB_TICKET, new String[] { "t_id", "uuid",
				"zone", "price", "pubkey", "validtime", "sig" }, "t_id=?",
				new String[] { String.valueOf(tId) }, null, null, null, null);

	}
	
	public boolean insertIntoStamp(int sId, Timestamp time, String sig, int smid) {
		String timeString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(time);
		ContentValues values = new ContentValues();
		values.put("s_id", sId);
		values.put("timestamp", timeString);
		values.put("sig", sig);
		values.put("smid", smid);
		long resp = mDb.insert(TAB_STAMP, null, values);
		values.clear();

		return resp > -1;
	}
	
	public void deleteStamp(int sId) {
		mDb.delete(TAB_STAMP, "s_id=" + sId, null);
	}
	
	public Cursor retriveStampById(int sId) throws SQLException {
		return mDb.query(true, TAB_STAMP, new String[] { "s_id", "timestamp", "sig", "smid"}, "s_id=?",
				new String[] { String.valueOf(sId) }, null, null, null, null);
	}
	

	private static class DatabaseHelper extends SQLiteOpenHelper {

		public DatabaseHelper(Context context, String user) {
			super(context, DB_NAME+user, null, DATABASE_VERSION);
		}

		@Override
		public void onOpen(SQLiteDatabase db) {
			super.onOpen(db);
			if (!db.isReadOnly()) {
				db.execSQL("PRAGMA foreign_keys=ON;");
			}
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(CREATETICKET);
			db.execSQL(CREATESTAMP);

		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL("DROP TABLE IF EXISTS " + TAB_TICKET);
			db.execSQL("DROP TABLE IF EXISTS " + TAB_STAMP);
			onCreate(db);
		}

	}

}

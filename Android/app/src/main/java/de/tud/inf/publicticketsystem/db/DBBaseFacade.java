package de.tud.inf.publicticketsystem.db;

import java.sql.Timestamp;
import java.util.ArrayList;

import de.tud.inf.publicticketsystem.model.Stamp;
import de.tud.inf.publicticketsystem.model.Ticket;

import android.content.Context;
import android.database.Cursor;


public class DBBaseFacade {
	
	private DBAdapter db;
	
	public DBBaseFacade(Context ctx, String user) {

		db = new DBAdapter(ctx, user);

	}
	
	public boolean insertIntoTicket(String uuid, int zone, String price, String pubKey, int validTime, String sig) {
		db.open();
		boolean resp = db.insertIntoTicket(uuid, zone, price, pubKey, validTime, sig);
		db.close();
		
		return resp;
	}
	
	public void deleteTicket(int tId) {
		db.open();
		db.deleteTicket(tId);
		db.close();
	}
	
	public ArrayList<Ticket> retriveAllTicket() {
		db.open();
		ArrayList<Ticket> mList = new ArrayList<Ticket>();
		Cursor c = db.retriveAllTickets();
		if (c != null && c.moveToFirst()) {
			do {
				int tId = c.getInt(c.getColumnIndex("t_id"));
				String uuid = c.getString(c.getColumnIndex("uuid"));
				int zone = c.getInt(c.getColumnIndex("zone"));
				String price = c.getString(c.getColumnIndex("price"));
				String pubKey = c.getString(c.getColumnIndex("pubkey"));
				int validTime = c.getInt(c.getColumnIndex("validtime"));
				String sig = c.getString(c.getColumnIndex("sig"));

				Ticket tRetrive = new Ticket(tId, uuid, zone, price, pubKey, validTime);
				tRetrive.setSig(sig);
				mList.add(tRetrive);

			} while (c.moveToNext());
		}
		c.close();
		db.close();
		return mList;
	}
	
	public Ticket retriveTicketById(int tId) {
		db.open();
		Ticket tRetrive = null;
		Cursor c = db.retriveTicketById(tId);
		if (c != null && c.moveToFirst()) {
			do {
				int tlId = c.getInt(c.getColumnIndex("t_id"));
				String uuid = c.getString(c.getColumnIndex("uuid"));
				int zone = c.getInt(c.getColumnIndex("zone"));
				String price = c.getString(c.getColumnIndex("price"));
				String pubKey = c.getString(c.getColumnIndex("pubkey"));
				int validTime = c.getInt(c.getColumnIndex("validtime"));
				String sig = c.getString(c.getColumnIndex("sig"));

				tRetrive = new Ticket(tlId, uuid, zone, price, pubKey, validTime);
				tRetrive.setSig(sig);
			} while (c.moveToNext());
		}
		c.close();
		db.close();
		return tRetrive;
	}
	
	public boolean insertIntoStamp(int sId, Timestamp time, String sig, int smid) {
		db.open();
		boolean resp = db.insertIntoStamp(sId, time, sig, smid);
		db.close();
		
		return resp;
	}
	
	public void deleteStamp(int sId) {
		db.open();
		db.deleteStamp(sId);
		db.close();
	}
	
	public Stamp retriveStampById(int sId) {
		db.open();
		Stamp sRetrive = null;
		Cursor c = db.retriveStampById(sId);
		if (c != null && c.moveToFirst()) {
			do {
				int stId = c.getInt(c.getColumnIndex("s_id"));
				Timestamp time = Timestamp.valueOf(c.getString(c.getColumnIndex("timestamp")));
				String sig = c.getString(c.getColumnIndex("sig"));
				int smid = c.getInt(c.getColumnIndex("smid"));

				sRetrive = new Stamp(stId, time, sig, smid);

			} while (c.moveToNext());
		}
		db.close();
		c.close();
		return sRetrive;
	}

}

package de.tud.inf.publicticketsystem.activity;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.readystatesoftware.systembartint.SystemBarTintManager;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import de.tud.inf.publicticketsystem.R;
import de.tud.inf.publicticketsystem.controller.NfcController;
import de.tud.inf.publicticketsystem.controller.TicketController;
import de.tud.inf.publicticketsystem.db.DBBaseFacade;
import de.tud.inf.publicticketsystem.model.Ticket;
import de.tud.inf.publicticketsystem.security.Cryptography;
import de.tud.inf.publicticketsystem.security.RSA;
import de.tud.inf.publicticketsystem.security.SecureStorage;
import de.tud.inf.publicticketsystem.util.StringUtil;

public class TicketDetails extends Activity {

    private DBBaseFacade dbSqlite;
    private NfcController nfcInstance = NfcController.getInstance();
    private TicketController ticketInstance = TicketController.getInstance();
    private NfcAdapter nfcAdapter;
    private boolean stampPressed = false;
    private boolean stamped = false;
    private boolean checked = false;
    private boolean checkPressed = false;
    private boolean stopProcess = false;
    private Ticket myTicket;
    private ProgressDialog dialog;
    private Handler handler = new Handler();
    private SecureStorage dbFlash = SecureStorage.getInstace();
    private CountDownTimer mCountDown;

    TextView tvTicketStampedDate;
    TextView tvTicketStampedTime;
    TextView tvStampExpires;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_details_2);
        SystemBarTintManager tintManager = new SystemBarTintManager(this);
        tintManager.setStatusBarTintEnabled(true);
        tintManager.setStatusBarTintColor(getResources().getColor(R.color.blue_light));

        dbSqlite = new DBBaseFacade(getApplicationContext(),
                dbFlash.retriveConfig(getApplicationContext(),
                        SecureStorage.USERREMEMBER));

        Intent intent = getIntent();
        int ticketID = intent.getIntExtra("ticket_id", 0);

        myTicket = dbSqlite.retriveTicketById(ticketID);
        String strSigStamp = ticketInstance.getStampSigOnDB(
                dbSqlite.retriveStampById(myTicket.getId()));
        myTicket.setSig(strSigStamp);

        TextView tvTicketZone = (TextView) findViewById(R.id.tvTicketZone);
        TextView tvTicketPrice = (TextView) findViewById(R.id.tvTicketPrice);
        TextView tvTicketTime = (TextView) findViewById(R.id.tvTicketName);
        tvTicketStampedDate = (TextView) findViewById(R.id.tvStamped_date);
        tvTicketStampedTime = (TextView) findViewById(R.id.tvStamped_time);
        tvStampExpires = (TextView) findViewById(R.id.tvExpires);

        tvTicketZone.setText(ticketInstance.convertTicketZone(myTicket.getZone()));
        tvTicketPrice.setText(myTicket.getPrice());
        tvTicketTime.setText(ticketInstance.convertTicketTime(myTicket.getValidTime()));
        String strTimeStamp = ticketInstance.getStampTimeOnDB(dbSqlite.retriveStampById(
                myTicket.getId()));

        if (strTimeStamp.contains(" ") && !strTimeStamp.equals("Not Stamped")) {
            String[] explited = strTimeStamp.split(" ");
            tvTicketStampedDate.setText(explited[0]);
            tvTicketStampedTime.setText(explited[1]);
        } else {
            tvTicketStampedDate.setText("- -/- -/- - - -");
            tvTicketStampedTime.setText("- -:- -");
        }

        deleteStampBehavior(myTicket);

    }

    private void deleteStampBehavior(final Ticket myTicket) {
        final boolean stampValid = ticketInstance.hasValidStamp(ticketInstance
                .getStampTimeOnDB(dbSqlite.retriveStampById(myTicket.getId())));

        Button btnStamp = (Button) findViewById(R.id.btnStamp);
        if (stampValid) {
            String time = ticketInstance.getTicketRemainingTime(myTicket.getValidTime(),
                    dbSqlite.retriveStampById(myTicket.getId()));
            if (time.equals("Expired")) {
                btnStamp.setEnabled(false);
                tvStampExpires.setText("Expired");
            } else {
                final long millis = ticketInstance.getTicketRemainingTimeInLong(myTicket.getValidTime(),
                        dbSqlite.retriveStampById(myTicket.getId()));
                mCountDown = new CountDownTimer(millis, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        StringBuilder sb = new StringBuilder();
                        long days = TimeUnit.MILLISECONDS.toDays(millisUntilFinished);
                        if (days != 0) {
                            sb.append(days);
                            if (days >= 2) {
                                sb.append(" days ");
                            } else {
                                sb.append(" day ");
                            }
                        }
                        sb.append(String.format("%02d:%02d:%02d",

                                TimeUnit.MILLISECONDS.toHours(millisUntilFinished) -
                                        TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millisUntilFinished)),
                                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) -
                                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)), // The change is in this line
                                TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                        tvStampExpires.setText(sb.toString());
                    }

                    @Override
                    public void onFinish() {
                        tvStampExpires.setText("Expired");
                    }
                };
                mCountDown.start();
            }
            btnStamp.setText("Check Ticket");
        } else {
            tvStampExpires.setText("Undefined");
        }
        btnStamp.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                SyncIncoData aSync;
                if (stampValid) {
                    checkPressed = true;
                    aSync = new SyncIncoData(
                            "Place your phone on the CHECK point");
                    aSync.execute(new String[]{myTicket.toString()});
                } else {
                    stampPressed = true;
                    aSync = new SyncIncoData(
                            "Place your phone on the STAMP point");
                    aSync.execute(new String[]{myTicket.toString()});
                }

            }
        });
    }


    private void doCheckOperation(Intent data) {

        String action = data.getAction();
        if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)) {
            try {
                long timeBefore = System.currentTimeMillis();

                Tag tag = data.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                Ndef ndefTag = Ndef.get(tag);
                nfcInstance.openConnection(ndefTag);

                // Send Modulus
                String ticketKey = myTicket.getPubKey();
                List<String> strSplited = splitTicketPubK(ticketKey, 240);
                for (int i = 0; i < strSplited.size(); i++) {
                    nfcInstance.writeTag(ndefTag, "M" + strSplited.get(i));
                }

                String nonce = generateNonce();
                nfcInstance.writeTag(ndefTag, "N" + nonce); // finish sending pubk

                //Receive Cert.
                String certificate = "";
                do {
                    nfcInstance.writeTag(ndefTag, "C");
                    certificate += nfcInstance.readNFCTag(ndefTag).replace("en", "");
                } while (!certificate.contains("|"));
                certificate = certificate.replace("|", "");

                Log.i("CERTIFICATE", certificate);

                //Receive CertSig;
                nfcInstance.writeTag(ndefTag, "Q");
                String certSig = nfcInstance.readNFCTag(ndefTag).replace("en", "");

                Log.i("CERTSIG", certSig);
                // TODO check Cert

                String challenge = "";
                do {
                    nfcInstance.writeTag(ndefTag, "F");
                    challenge += nfcInstance.readNFCTag(ndefTag).replace("en", "");
                } while (!challenge.contains("|"));
                challenge = challenge.replace("|", "");

                // TODO check sig challenge

                // Send Challenge Response hashed
                String privK = dbFlash.retriveConfig(getApplicationContext(),
                        SecureStorage.PRIVATEKEY);
                String modulus = dbFlash.retriveConfig(getApplicationContext(),
                        SecureStorage.MODULUS);
                String decrypted = answerChallenge(challenge, privK, modulus);

                String[] extractNonce = decrypted.split("990");
                if (!extractNonce[0].equals(nonce)) {
                    stopProcess = true;
                    nfcInstance.writeTag(ndefTag, "V"); // Abort
                    nfcInstance.closeConnection(ndefTag);
                    ndefTag.close();
                    return;
                }

                String nonceHashed = Cryptography.hashString(extractNonce[1]);
                nfcInstance.writeTag(ndefTag, "A" + nonceHashed);

                Log.i("DEC", extractNonce[1]);

                // challenge agreement.
                String challengeResponse = nfcInstance.readNFCTag(ndefTag);
                if (challengeResponse.contains("ok")) {
                    checked = true;
                    nfcInstance.writeTag(ndefTag, "X" + myTicket.toString());
                    nfcInstance.writeTag(ndefTag, "Z" + ticketInstance.getStampTimeOnDB(dbSqlite.retriveStampById(myTicket.getId())) + " ");
                    long timeAfter = System.currentTimeMillis();
                    long finalTime = timeAfter - timeBefore;
                    updateCheckView(challengeResponse, finalTime);
                } else {
                    stopProcess = true;
                }


            } catch (Exception ex) {
                // Sometimes the app crash because ex.getMessage() returns null.
                String msg = ex.getMessage();
                if (msg == null) {
                    msg = "No msg.";
                }
                Log.e("PUBLICTICKET-ERROR", msg);
            }

        }

    }

    private void doStampOperation(Intent data) {
        try {

            String action = data.getAction();
            if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)) {

                long timeBefore = System.currentTimeMillis();

                Tag tag = data.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                Ndef ndefTag = Ndef.get(tag);
                nfcInstance.openConnection(ndefTag);

                // Send Modulus
                String ticketKey = myTicket.getPubKey();
                List<String> strSplited = splitTicketPubK(ticketKey, 240);
                for (int i = 0; i < strSplited.size(); i++) {
                    nfcInstance.writeTag(ndefTag, "M" + strSplited.get(i));
                }

                String nonce = generateNonce();
                nfcInstance.writeTag(ndefTag, "N" + nonce); // finish sending pubk

                //Receive Cert.
                String certificate = "";
                do {
                    nfcInstance.writeTag(ndefTag, "C");
                    certificate += nfcInstance.readNFCTag(ndefTag).replace("en", "");
                } while (!certificate.contains("|"));
                certificate = certificate.replace("|", "");

                Log.i("CERTIFICATE", certificate);

                //Receive CertSig;
                nfcInstance.writeTag(ndefTag, "Q");
                String certSig = nfcInstance.readNFCTag(ndefTag).replace("en", "");

                Log.i("CERTSIG", certSig);
                // TODO check Cert

                String challenge = "";
                do {
                    nfcInstance.writeTag(ndefTag, "F");
                    challenge += nfcInstance.readNFCTag(ndefTag).replace("en", "");
                } while (!challenge.contains("|"));
                challenge = challenge.replace("|", "");

                // TODO check sig challenge

                // Send Challenge Response hashed
                String privK = dbFlash.retriveConfig(getApplicationContext(),
                        SecureStorage.PRIVATEKEY);
                String modulus = dbFlash.retriveConfig(getApplicationContext(),
                        SecureStorage.MODULUS);
                String decrypted = answerChallenge(challenge, privK, modulus);

                String[] extractNonce = decrypted.split("990");
                if (!extractNonce[0].equals(nonce)) {
                    stopProcess = true;
                    nfcInstance.writeTag(ndefTag, "V"); // Abort
                    nfcInstance.closeConnection(ndefTag);
                    ndefTag.close();
                    return;
                }

                String nonceHashed = Cryptography.hashString(extractNonce[1]);
                nfcInstance.writeTag(ndefTag, "A" + nonceHashed);

                Log.i("DEC", extractNonce[1]);

                // challenge agreement.
                String challengeResponse = nfcInstance.readNFCTag(ndefTag);

                if (challengeResponse.contains("ok")) {
                    // Stamping
                    String sig = myTicket.getSig();
                    myTicket.setSig(sig);
                    nfcInstance.writeTag(ndefTag, "S" + myTicket.toString());
                    String tagInfo = nfcInstance.readNFCTag(ndefTag);

                    stamped = true;
                    long timeAfter = System.currentTimeMillis();
                    long finalTime = timeAfter - timeBefore;
                    updateOutputView(tagInfo, finalTime);
                } else {
                    stopProcess = true;

                }
                nfcInstance.closeConnection(ndefTag);
                ndefTag.close();

            }
        } catch (Exception ex) {

            // Sometimes the app crash because ex.getMessage() returns null.
            String msg = ex.getMessage();
            if (msg == null) {
                msg = "No msg.";
            }
            Log.e("PUBLICTICKET-ERROR", msg);
        }
    }

    private String generateNonce() throws NoSuchAlgorithmException {
        Random rand = SecureRandom.getInstance("SHA1PRNG");
        int nonce = 0;
        do {
            nonce = rand.nextInt(100000000);
        } while (String.valueOf(nonce).contains("990"));
        return String.valueOf(nonce);
    }

    private List<String> splitTicketPubK(String s, int chunkSize) {
        List<String> result = new ArrayList<String>();
        int length = s.length();
        for (int i = 0; i < length; i += chunkSize) {
            result.add(s.substring(i, Math.min(length, i + chunkSize)));
        }
        return result;
    }

    private String answerChallenge(String code, String key, String modulus) {
        BigInteger bCode = new BigInteger(code);
        BigInteger bKey = new BigInteger(key);
        BigInteger bMod = new BigInteger(modulus);

        BigInteger dec = RSA.decrypt(bCode, bKey, bMod);

        return String.valueOf(dec);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    final Intent data) {
        switch (requestCode) {
            case NfcController.PENDING_INTENT_TECH_DISCOVERED:

                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        boolean stampValid = ticketInstance
                                .hasValidStamp(ticketInstance.getStampTimeOnDB(
                                        dbSqlite.retriveStampById(myTicket.getId())));
                        if (stampValid) {
                            doCheckOperation(data);
                        } else {
                            doStampOperation(data);
                        }
                    }
                };
                if (stampPressed || checkPressed) {
                    new Thread(runnable).start();
                }

                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.ticket_details, menu);
        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onPause() {
        super.onPause();
        // disable searching for tags
        nfcAdapter.disableForegroundDispatch(this);
        if (mCountDown != null) {
            mCountDown.cancel();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        // get the NFC-Adapter
        nfcAdapter = ((NfcManager) this.getSystemService(Context.NFC_SERVICE))
                .getDefaultAdapter();

        // PendingIntent is handling the discovery of Ndef and NdefFOrmatable
        // tags
        PendingIntent pi = createPendingResult(
                NfcController.PENDING_INTENT_TECH_DISCOVERED, new Intent(), 0);

        // set the tag types to search for
        nfcAdapter.enableForegroundDispatch(this, pi,
                new IntentFilter[]{new IntentFilter(
                        NfcAdapter.ACTION_TECH_DISCOVERED)}, new String[][]{
                        new String[]{"android.nfc.tech.NdefFormatable"},
                        new String[]{"android.nfc.tech.Ndef"}});
    }

    private void updateCheckView(final String str, final long time) {
        handler.post(new Runnable() {

            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), str.replace("en", "") + "\nTime: " + time, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void updateOutputView(final String str, final long time) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (checkPressed) {
                    Toast.makeText(getApplicationContext(), str,
                            Toast.LENGTH_LONG).show();
                } else {
                    boolean resp = insertTicketData(str);

                    if (resp) {
                        Toast.makeText(getApplicationContext(),
                                "Ticket Stamped in " + time + " ms",
                                Toast.LENGTH_LONG).show();
                        String[] explited = ticketInstance
                                .getStampTimeOnDB(dbSqlite
                                        .retriveStampById(myTicket.getId())).split(" ");
                        tvTicketStampedDate.setText(explited[0]);
                        tvTicketStampedTime.setText(explited[1]);
                        deleteStampBehavior(myTicket);

                    } else {
                        Toast.makeText(getApplicationContext(), "Error :(",
                                Toast.LENGTH_LONG).show();
                    }
                }

            }

            private boolean insertTicketData(final String str) {
                String[] fixedStr = StringUtil.fixResponse(str
                        .replace("en", ""));
                String sig = fixedStr[0];
                String timestamp = fixedStr[1].trim();
                int smid = Integer.valueOf(fixedStr[2]);

                String times = StringUtil.formatDateFromServer(timestamp);
                Timestamp ts = Timestamp.valueOf(times);

                boolean resp = dbSqlite.insertIntoStamp(myTicket.getId(), ts,
                        sig, smid);
                return resp;
            }
        });
    }

    private class SyncIncoData extends AsyncTask<String, String, String> {

        private String msg;

        public SyncIncoData(String msg) {
            this.msg = msg;
        }

        @Override
        protected String doInBackground(String... params) {

            String ticket = params[0];
            int timeout = 100;

            while ((!stamped && !checked) && timeout != 0 && !stopProcess) {
                try {
                    Thread.sleep(200);
                    timeout--;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            if (timeout == 0 && !stopProcess) {
                stamped = false;
                checked = false;
                return "timeout";
            } else if (stopProcess) {
                return "checkError";
            } else {
                return ticket;
            }
        }

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

            if (dialog != null) {
                dialog = null;
            }
            dialog = new ProgressDialog(TicketDetails.this);
            dialog.setMessage(this.msg);
            dialog.show();

        }

        @Override
        protected void onPostExecute(String response) {

            if (dialog.isShowing()) {
                dialog.dismiss();
            }

            if (response.equals("timeout")) {
                Toast.makeText(getApplicationContext(), "Timeout :(",
                        Toast.LENGTH_LONG).show();
            } else if (response.contains("checkError")) {
                Toast.makeText(getApplicationContext(),
                        "Falied to check identity", Toast.LENGTH_LONG).show();
            }

        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

    }

}

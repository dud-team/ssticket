package de.tud.inf.publicticketsystem.security;

import android.content.Context;
import android.content.SharedPreferences;

public class SecureStorage {

	public static final String PRIVATEKEY = "privateKey";
	public static final String PUBLICKEY = "publicKey";
	public static final String MODULUS = "modulus";
	public static final String GENKEY = "genkeys";
	public static final String KEYLEN = "keylen";
	public static final String KEYTYPE = "keytype";

	public static String USERREMEMBER = "userRemember";
	public static String USERCONF = "userConf";
	private static SecureStorage instance;
	private final String LOCALFILE = "dbfile";

	public final static String MODULUSROOTKEY = "2624109163931304156529865434557355301719563847283" +
			"688092016632754339276486435817936652774653401287971070516577549157525058389688" +
			"178275722343501385180306290975609692187352691948135635781261896224024747333166" +
			"718212202103110723130885068745929607227501631830984373511360315070902649432552" +
			"780797225243236313179942607532883682114000004181997997373435320450274765014146" +
			"727651983196146934520355907608793129044704040255030123518587661880759497064982" +
			"670780531692365939674216888312183983079743888360291793912874445731838918006527" +
			"758396849208742495364510947128404710060740837086953250827396061163250942163736" +
			"0223953795069494161051";

	private SecureStorage() {
	}

	public static SecureStorage getInstace() {
		if (instance == null) {
			instance = new SecureStorage();
		}
		return instance;
	}

	public void saveConfig(Context ctx, String attr, String file) {
		SharedPreferences settings = ctx.getApplicationContext()
				.getSharedPreferences(LOCALFILE, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(file, encrypt(attr));

		editor.commit();
	}

	public String retriveConfig(Context ctx, String file) {
		SharedPreferences settings = ctx.getApplicationContext()
				.getSharedPreferences(LOCALFILE, 0);
		return decrypt(settings.getString(file, ""));
	}

	public String encrypt(String msg) {
		// TODO
		return msg;
	}

	public String decrypt(String cipher) {
		// TODO
		return cipher;
	}

	public void saveConfig(String uSave, Context ctx, String file) {
		SharedPreferences settings = ctx.getApplicationContext()
				.getSharedPreferences(LOCALFILE, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(file, encrypt(uSave));

		editor.commit();

	}

}

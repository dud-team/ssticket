package de.tud.inf.publicticketsystem.controller;

import java.math.BigInteger;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import de.tud.inf.publicticketsystem.model.User;
import de.tud.inf.publicticketsystem.network.HttpRequest;
import de.tud.inf.publicticketsystem.security.Cryptography;
import de.tud.inf.publicticketsystem.security.RSA;
import de.tud.inf.publicticketsystem.security.SecureStorage;

public class UserController {

	public static UserController instance;

	private final String LOGINURL = HttpRequest.SERVER + HttpRequest.WEBSERVICE
			+ "/user/checkLogin.php";
	private final String REGISTERURL = HttpRequest.SERVER
			+ HttpRequest.WEBSERVICE + "/user/register.php";
	private final String SEARCHUSER = HttpRequest.SERVER
			+ HttpRequest.WEBSERVICE + "/user/listUser.php";
	private final String GETPROFILE = HttpRequest.SERVER
			+ HttpRequest.WEBSERVICE + "/user/getLogin.php";

	private SecureStorage mStorage = SecureStorage.getInstace();

	private UserController() {

	}

	public static UserController getInstance() {
		if (instance == null) {
			instance = new UserController();
		}
		return instance;
	}

	public User getUserProfile(int u_id) {
		String response = HttpRequest.requestHttp(String.format("%s?u_id=%s",
				GETPROFILE, u_id));
		User u = null;
		try {
			JSONArray jArray = new JSONArray(response);
			for (int i = 0; i < jArray.length(); i++) {
				JSONObject jObject = jArray.getJSONObject(i);

				String uid = jObject.getString("u_id");
				String uEmail = jObject.getString("email");
				String uName = jObject.getString("name");
				String uLastName = jObject.getString("last_name");
				u = new User(Integer.valueOf(uid), uName, uLastName, uEmail);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return u;
	}

	public String getLoginUser(String email, String pass) {
		String passwordEncrypt = Cryptography.hashString(pass);
		String response = HttpRequest.requestHttp(String.format(
				"%s?email=%s&pass=%s", LOGINURL, email, passwordEncrypt));

		if (response.equals("true")) {
			String joinUserConf = "";
			try {
				String userData = HttpRequest.requestHttp(String.format(
						"%s?email=%s", GETPROFILE, email));

				JSONArray jArray = new JSONArray(userData);
				for (int i = 0; i < jArray.length(); i++) {
					JSONObject jObject = jArray.getJSONObject(i);

					String uid = jObject.getString("u_id");
					String uEmail = jObject.getString("email");
					String uName = jObject.getString("name");
					String uLastName = jObject.getString("last_name");

					joinUserConf = String.format("%s#%s#%s#%s#", uid, uEmail,
							uName, uLastName);

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return joinUserConf;
		}
		return response;
	}

	public Boolean registerUser(String name, String lastName, String email,
			String password) {
		String passwordEncrypt = Cryptography.hashString(password);
		String response = HttpRequest.requestHttp(String.format(
				"%s?name=%s&lastName=%s&email=%s&pass=%s", REGISTERURL, name,
				lastName, email, passwordEncrypt).replace(" ", "%20"));
		if (response.equals("true")) {
			return true;
		}
		return false;
	}

	public ArrayList<User> searchUser(String name) {
		String response = HttpRequest.requestHttp(String.format("%s?name=%s",
				SEARCHUSER, name));
		ArrayList<User> sUsersList = new ArrayList<User>();
		try {
			JSONArray jArray = new JSONArray(response);
			for (int i = 0; i < jArray.length(); i++) {
				JSONObject jObject = jArray.getJSONObject(i);

				String id = jObject.getString("u_id");
				String uEmail = jObject.getString("email");
				String uName = jObject.getString("name");
				String uLastName = jObject.getString("last_name");

				sUsersList.add(new User(Integer.valueOf(id), uName, uLastName,
						uEmail));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return sUsersList;
	}

	public void genKeys(Context ctx) {

		String keyLen = mStorage.retriveConfig(ctx, SecureStorage.KEYLEN);
		BigInteger[] myKeys;
		if (keyLen == "") {
			mStorage.saveConfig(ctx, String.valueOf(RSA.DEFAULTLENGTH), SecureStorage.KEYLEN);
			myKeys = RSA.genKeys(RSA.DEFAULTLENGTH);
		} else {
			myKeys = RSA.genKeys(Integer.valueOf(keyLen));
		}

		mStorage.saveConfig(ctx, String.valueOf(myKeys[0]),
				SecureStorage.MODULUS);
		mStorage.saveConfig(ctx, String.valueOf(myKeys[1]),
				SecureStorage.PUBLICKEY);
		mStorage.saveConfig(ctx, String.valueOf(myKeys[2]),
				SecureStorage.PRIVATEKEY);
		mStorage.saveConfig(ctx, "DONE", SecureStorage.GENKEY);

	}

}

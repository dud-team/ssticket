package de.tud.inf.publicticketsystem.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import de.tud.inf.publicticketsystem.R;
import de.tud.inf.publicticketsystem.controller.UserController;
import de.tud.inf.publicticketsystem.security.SecureStorage;

public class SettingActivity extends Activity {

	private SecureStorage dbFlash = SecureStorage.getInstace();
	private UserController userInstance = UserController.getInstance();

	TextView tvPub;
	TextView tvPri;
	TextView modulus;
	TextView tvKeyLen;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);

		tvPub = (TextView) findViewById(R.id.tvSetPubK);
		tvPri = (TextView) findViewById(R.id.tvSetPrivK);
		modulus = (TextView) findViewById(R.id.tvSetModulus);
		tvKeyLen = (TextView) findViewById(R.id.tvSetKeyLength);

		Button reset = (Button) findViewById(R.id.btnResetKeys);
		Button changeKeyLen = (Button) findViewById(R.id.btnChangeKeyLen);

		updateKeysOnScreen();
		

		changeKeyLen.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// get prompts.xml view
				LayoutInflater li = LayoutInflater.from(v.getContext());
				View promptsView = li.inflate(R.layout.prompts, null);

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						v.getContext());

				// set prompts.xml to alertdialog builder
				alertDialogBuilder.setView(promptsView);

				final EditText userInput = (EditText) promptsView
						.findViewById(R.id.editTextDialogUserInput);

				// set dialog message
				alertDialogBuilder
						.setCancelable(false)
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										dbFlash.saveConfig(getApplicationContext(), userInput.getText().toString(), SecureStorage.KEYLEN);
										updateKeys();
										updateKeysOnScreen();
									}
								})
						.setNegativeButton("Cancel",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								});

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();
			}
		});

		reset.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				updateKeys();
				updateKeysOnScreen();
			}

			
		});

	}
	
	private void updateKeys() {
		userInstance.genKeys(getApplicationContext());
		Toast.makeText(getApplicationContext(), "New Keys installed",
				Toast.LENGTH_SHORT).show();
	}

	private void updateKeysOnScreen() {
		String pub = dbFlash.retriveConfig(getApplicationContext(),
				SecureStorage.PUBLICKEY);
		String pri = dbFlash.retriveConfig(getApplicationContext(),
				SecureStorage.PRIVATEKEY);
		String mod = dbFlash.retriveConfig(getApplicationContext(),
				SecureStorage.MODULUS);
		String keyLen = dbFlash.retriveConfig(getApplicationContext(),
				SecureStorage.KEYLEN);
		Log.i("MODULUS", mod);
		Log.i("PRIV", pri);
		
		tvPub.setText(pub);
		tvPri.setText(pri);
		modulus.setText(mod);
		tvKeyLen.setText(keyLen);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.setting, menu);
		return true;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}

}

package de.tud.inf.publicticketsystem.exceptions;

public class SpaceFullTagException extends Exception {


	private static final long serialVersionUID = 6598805742339685238L;
	
	public SpaceFullTagException() {
		super();
	}
	
	@Override
	public String getMessage() {
		return "No space left on the tag";
	}

}

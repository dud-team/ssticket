package de.tud.inf.publicticketsystem.exceptions;

public class ReadOnlyTagException extends Exception{
	
	
	private static final long serialVersionUID = -512432549078174007L;
	public ReadOnlyTagException() {
		super();
	}
	@Override
	public String getMessage() {
		return "Tag is Readonly";
	}

}

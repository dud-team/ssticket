package de.tud.inf.publicticketsystem.activity;

import de.tud.inf.publicticketsystem.R;
import de.tud.inf.publicticketsystem.controller.NfcController;
import de.tud.inf.publicticketsystem.db.DBBaseFacade;
import de.tud.inf.publicticketsystem.security.SecureStorage;
import de.tud.inf.publicticketsystem.util.StringUtil;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.Menu;
import android.widget.TextView;

public class BuyTicket extends Activity {
	
	private TextView statusOutput;
	private Handler handler = new Handler();
	
	private NfcController nfcInstance = NfcController.getInstance();
	private NfcAdapter nfcAdapter;
	
	private DBBaseFacade dbSqlite;
	private SecureStorage dbFlash = SecureStorage.getInstace();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_buy_ticket);
		
		dbSqlite = new DBBaseFacade(getApplicationContext(), dbFlash.retriveConfig(getApplicationContext(), SecureStorage.USERREMEMBER));
		
		statusOutput = (TextView) findViewById(R.id.tvStatus);
		
		NfcAdapter adapter = ((NfcManager) getSystemService(Context.NFC_SERVICE))
				.getDefaultAdapter();
		checkNfcEnable(adapter);
		
	}
	
	private void doTagOperation(Intent data) {
		try {

			String action = data.getAction();
			if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)) {
				
				Tag tag = data.getParcelableExtra(NfcAdapter.EXTRA_TAG);
				Ndef ndefTag = Ndef.get(tag);
				nfcInstance.writeTag(ndefTag, "1461gsaEmt|10|0210|PUB|2|150|sig|");
				String tagInfo = nfcInstance.readNFCTag(ndefTag);
				updateOutputView(tagInfo);
			}
		} catch (Exception ex) {

			// Sometimes the app crash because ex.getMessage() returns null.
			String msg = ex.getMessage();
			if (msg == null) {
				msg = "No msg.";
			}
		}
	}
	
	// happens when a tag is discovered
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			final Intent data) {
		switch (requestCode) {
		case NfcController.PENDING_INTENT_TECH_DISCOVERED:

			Runnable runnable = new Runnable() {
				@Override
				public void run() {
					doTagOperation(data);
				}
			};
			new Thread(runnable).start();

			break;
		}
	}
	
	@Override
	public void onPause() {
		super.onPause();
		// disable searching for tags
		nfcAdapter.disableForegroundDispatch(this);
	}

	
	@Override
	public void onResume() {
		super.onResume();

		// get the NFC-Adapter
		nfcAdapter = ((NfcManager) this.getSystemService(Context.NFC_SERVICE))
				.getDefaultAdapter();

		checkNfcEnable(nfcAdapter);

		// PendingIntent is handling the discovery of Ndef and NdefFOrmatable
		// tags
		PendingIntent pi = createPendingResult(NfcController.PENDING_INTENT_TECH_DISCOVERED,
				new Intent(), 0);

		// set the tag types to search for
		nfcAdapter.enableForegroundDispatch(this, pi,
				new IntentFilter[] { new IntentFilter(
						NfcAdapter.ACTION_TECH_DISCOVERED) }, new String[][] {
						new String[] { "android.nfc.tech.NdefFormatable" },
						new String[] { "android.nfc.tech.Ndef" } });
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.buy_ticket, menu);
		return true;
	}
	
	private void updateOutputView(final String str) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				
				String[] fixedStr = StringUtil.fixResponse(str);
				String uuid = fixedStr[0];
				int zone = Integer.valueOf(fixedStr[1]);
				String price = fixedStr[2];
				String key = fixedStr[3];
				int validTime = Integer.valueOf(fixedStr[4]);
				String sig = fixedStr[5];
				
				//TODO Move this piece of code to a method.
				boolean resp = dbSqlite.insertIntoTicket(uuid, zone, price, key, validTime, sig);
				if(resp) {
					statusOutput.setText("Ticket Added");
				}else {
					statusOutput.setText("Error :(");
				}
				
			}
		});
	}
	
	
	private void checkNfcEnable(NfcAdapter adapter) {
		if (adapter == null || !adapter.isEnabled()) {
			statusOutput.setText("NFC is turned off. :(");
		} else {
			statusOutput.setText("Ready for take off.");
		}
	}

}

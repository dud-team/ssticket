package de.tud.inf.publicticketsystem.model;

import java.sql.Timestamp;


public class Stamp {

	private Timestamp time;
	private int sId;
	private int smid;
	private String sig;

	public Stamp(int stId, Timestamp time, String sig, int smid) {
		this.setsId(stId);
		this.setTime(time);
		this.setSig(sig);
		this.setSmid(smid);
	}
	
	public int getSmid() {
		return smid;
	}

	public String getSig() {
		return sig;
	}

	private void setSmid(int smid) {
		this.smid = smid;
	}

	private void setSig(String sig) {
		this.sig = sig;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	public int getsId() {
		return sId;
	}

	public void setsId(int sId) {
		this.sId = sId;
	}
	
	@Override
	public String toString() {
		
		return this.time.toString().substring(0, 19);
 	}

}

package de.tud.inf.publicticketsystem.activity;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.spongycastle.util.encoders.Base64;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import de.tud.inf.publicticketsystem.R;
import de.tud.inf.publicticketsystem.controller.TicketController;
import de.tud.inf.publicticketsystem.db.DBBaseFacade;
import de.tud.inf.publicticketsystem.model.Ticket;
import de.tud.inf.publicticketsystem.security.SecureStorage;

public class OnlineTicketActivity extends Activity {

	private Spinner spZones;
	private Spinner spTicket;
	private TextView price;
	private Button confirm;
	private DBBaseFacade dbSqlite;
	private TicketController ticketInstance = TicketController.getInstance();
	private SecureStorage dbFlash = SecureStorage.getInstace();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_generate_ticket);

		dbSqlite = new DBBaseFacade(getApplicationContext(), dbFlash.retriveConfig(getApplicationContext(), SecureStorage.USERREMEMBER));

		spZones = (Spinner) findViewById(R.id.spZone);
		spTicket = (Spinner) findViewById(R.id.spTicketType);
		price = (TextView) findViewById(R.id.tvPriceValue);
		confirm = (Button) findViewById(R.id.btnConfirm);

		List<String> listZones = loadZones();
		List<String> listTickets = loadTickets();

		ArrayAdapter<String> dataAdapterZones = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, listZones);
		dataAdapterZones
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spZones.setAdapter(dataAdapterZones);

		ArrayAdapter<String> dataAdapterTicket = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, listTickets);
		dataAdapterTicket
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spTicket.setAdapter(dataAdapterTicket);

		spZones.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onNothingSelected(AdapterView<?> parentView) {
				// Do nothing
			}

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				updatePriceTextView();

			}

		});

		spTicket.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onNothingSelected(AdapterView<?> parentView) {
				// Do nothing
			}

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				updatePriceTextView();

			}

		});

		confirm.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				long before = System.currentTimeMillis();
				storeTicket();
				long after = System.currentTimeMillis();
				long result = after - before;
				Toast.makeText(getApplicationContext(), String.valueOf(result), Toast.LENGTH_LONG).show();
				finish();
			}
		});

	}

	private boolean storeTicket() {
		String uuid = genUuidTicket();
		int zone = spZones.getLastVisiblePosition();
		String pubKey = getUserPublicKey();
		int validTime = spTicket.getLastVisiblePosition();
		String price = calculateTicketPrice();
		Ticket myTicket = new Ticket(1, uuid, zone, price, pubKey, validTime);
		return dbSqlite.insertIntoTicket(genUuidTicket(), myTicket.getZone(),
				myTicket.getPrice(), myTicket.getPubKey(),
				myTicket.getValidTime(), new String(Base64.encode(ticketInstance.signTicket(myTicket).getBytes())));

	}

	private String getUserPublicKey() {
		return new String(dbFlash.retriveConfig(getApplicationContext(), SecureStorage.MODULUS));
	}

	private String genUuidTicket() {
		return UUID.randomUUID().toString();
	}

	private String calculateTicketPrice() {
		int ticket = spTicket.getLastVisiblePosition();
		int zones = spZones.getLastVisiblePosition();
		double value = 0.0;

		switch (ticket) {
		case 0:
			value = 2 * (zones + 1);
			break;
		case 1:
			value = 5 * (zones + 1);
			break;
		case 2:
			value = 7 * (zones + 1);
			break;
		case 3:
			value = 9 * (zones + 1);
			break;
		}
		return String.valueOf(value);
	}

	private void updatePriceTextView() {
		price.setText(String.valueOf(calculateTicketPrice()));
	}

	private List<String> loadTickets() {
		List<String> listTickets = new ArrayList<String>();
		listTickets.add("Hour Ticket");
		listTickets.add("Day Ticket");
		listTickets.add("Weekly Ticket");
		listTickets.add("Monthly Ticket");
		return listTickets;
	}

	private List<String> loadZones() {
		List<String> listZones = new ArrayList<String>();
		listZones.add("City Center");
		listZones.add("Regional 1");
		listZones.add("Regional 2");
		return listZones;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.generate_ticket, menu);
		return true;
	}

}

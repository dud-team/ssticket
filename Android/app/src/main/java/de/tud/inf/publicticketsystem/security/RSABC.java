package de.tud.inf.publicticketsystem.security;

import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;

import javax.crypto.Cipher;

public class RSABC {

	public final static int DEFAULTLENGTH = 2048;
	public final static String XFORM = "RSA";

	public static KeyPair generateRSAKeyPair() throws GeneralSecurityException {
		KeyPairGenerator gen = KeyPairGenerator.getInstance("RSA");
		gen.initialize(DEFAULTLENGTH);
		return gen.generateKeyPair();
	}

	public static byte[] encrypt(byte[] plaintext, PublicKey pub)
			throws GeneralSecurityException {
		Cipher cipher = Cipher.getInstance(XFORM);
		cipher.init(Cipher.ENCRYPT_MODE, pub);
		return cipher.doFinal(plaintext);
	}

	public static byte[] decrypt(byte[] ciphertext, PrivateKey pvt)
			throws GeneralSecurityException {
		Cipher cipher = Cipher.getInstance(XFORM);
		cipher.init(Cipher.DECRYPT_MODE, pvt);
		return cipher.doFinal(ciphertext);
	}

}

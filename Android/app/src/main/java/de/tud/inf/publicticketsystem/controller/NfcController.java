package de.tud.inf.publicticketsystem.controller;

import java.io.IOException;
import java.nio.charset.Charset;

import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.TagLostException;
import android.nfc.tech.Ndef;
import android.util.Log;
import de.tud.inf.publicticketsystem.exceptions.ReadOnlyTagException;
import de.tud.inf.publicticketsystem.exceptions.SpaceFullTagException;
import de.tud.inf.publicticketsystem.model.InteractiveNFC;

public class NfcController {

	private static NfcController instance;
	private InteractiveNFC iNFC;
	public static final int PENDING_INTENT_TECH_DISCOVERED = 1;

	private NfcController() {
	}

	public static NfcController getInstance() {
		if (instance == null) {
			instance = new NfcController();
			return instance;
		}
		return instance;
	}

	public void openConnection(Ndef ndefTag) {
		try {
			iNFC = new InteractiveNFC(ndefTag);
			iNFC.openConnection();
		} catch (IOException e) {
			Log.i("ERROR", "I/O Error");
		}
	}

	public void closeConnection(Ndef ndefTag) {
		try {
			iNFC = new InteractiveNFC(ndefTag);
			iNFC.closeConnection();
		} catch (IOException e) {
			Log.i("ERROR", "I/O Error");
		}
	}

	public String readNFCTag(Ndef ndefTag) {

		try {
			iNFC = new InteractiveNFC(ndefTag);
			String nfcContent = iNFC.readTag();
			return nfcContent;

		} catch (IOException e) {
			Log.i("ERROR", "I/O Error");

		} catch (FormatException e) {
			Log.i("ERROR", "Format Error");

		}
		return "";
	}
	
	public boolean writeTagAsByte(Ndef ndefTag, byte[] tagInfo) {
		try {
				byte[] payload = new byte[tagInfo.length + 3];
				payload[0] = (byte) 0x02; // UTF-8
				payload[1] = (byte) 0x65; // e
				payload[2] = (byte) 0x6e; // n

				System.arraycopy(tagInfo, 0, payload, 3, tagInfo.length);

				NdefRecord record = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
						NdefRecord.RTD_TEXT, new byte[0], payload);
				NdefMessage msg = new NdefMessage(new NdefRecord[] { record });

				iNFC = new InteractiveNFC(ndefTag);
				iNFC.writeTag(msg);

			} catch (ReadOnlyTagException rote) {
				Log.i("ERROR", rote.getMessage());
				return false;
			}

			catch (SpaceFullTagException sfte) {
				Log.i("ERROR", sfte.getMessage());
				return false;
			}

			catch (TagLostException tle) {
				Log.i("ERROR", "Lost connection with the tag");
				return false;

			} catch (IOException ioe) {
				Log.i("ERROR", "I/O Error");
				return false;

			} catch (Exception e) {
				Log.i("ERROR", "Undefined Error");
				return false;
			}
			return true;
	}

	public boolean writeTag(Ndef ndefTag, String tagInfo) {
		try {
				if (tagInfo.startsWith("en")) {
					tagInfo = tagInfo.substring(2);
				}
	
				byte[] writeBytes = String.valueOf(tagInfo).getBytes(
						Charset.forName("UTF-8"));
				byte[] payload = new byte[writeBytes.length + 3];
				payload[0] = (byte) 0x02; // UTF-8
				payload[1] = (byte) 0x65; // e
				payload[2] = (byte) 0x6e; // n
	
				System.arraycopy(writeBytes, 0, payload, 3, writeBytes.length);
	
				NdefRecord record = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
						NdefRecord.RTD_TEXT, new byte[0], payload);
				NdefMessage msg = new NdefMessage(new NdefRecord[] { record });
	
				iNFC = new InteractiveNFC(ndefTag);
				iNFC.writeTag(msg);

		} catch (ReadOnlyTagException rote) {
			Log.i("ERROR", rote.getMessage());
			return false;
		}

		catch (SpaceFullTagException sfte) {
			Log.i("ERROR", sfte.getMessage());
			return false;
		}

		catch (TagLostException tle) {
			Log.i("ERROR", "Lost connection with the tag");
			return false;

		} catch (IOException ioe) {
			Log.i("ERROR", "I/O Error");
			return false;

		} catch (Exception e) {
			Log.i("ERROR", "Undefined Error");
			return false;
		}
		return true;
	}

}

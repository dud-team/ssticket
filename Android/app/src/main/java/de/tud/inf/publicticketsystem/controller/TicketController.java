package de.tud.inf.publicticketsystem.controller;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.spongycastle.util.encoders.Base64;

import de.tud.inf.publicticketsystem.model.Stamp;
import de.tud.inf.publicticketsystem.model.Ticket;
import de.tud.inf.publicticketsystem.network.HttpRequest;
import de.tud.inf.publicticketsystem.security.Cryptography;
import de.tud.inf.publicticketsystem.util.TimeUtil;

public class TicketController {

	public static TicketController instance;

	private final String SIGNTICKET = HttpRequest.SERVER
			+ HttpRequest.WEBSERVICE + "/ticket/signTicket.php";

	private TicketController() {

	}

	public static TicketController getInstance() {
		if (instance == null) {
			instance = new TicketController();
		}
		return instance;
	}

	public String signTicket(Ticket myTicket) {

		return HttpRequest.requestHttp(String.format(
				"%s?uuid=%s&zone=%s&price=%s&pubKey=%s&validTime=%s",
				SIGNTICKET, myTicket.getUuid(), myTicket.getZone(),
				myTicket.getPrice(), myTicket.getPubKey(),
				myTicket.getValidTime()));

	}

	public String convertTicketZone(int zone) {
		String out = "";

		switch (zone) {
		case 0:
			out = "City Center";
			break;
		case 1:
			out = "Regional 1";
			break;
		case 2:
			out = "Regional 2";
			break;

		}
		return out;
	}

	public long convertTicketTypeToHours(int validTime) {
		long out = 0;
		switch (validTime) {
		case 0:
			out = 3600;
			break;
		case 1:
			out = 86400;
			break;
		case 2:
			out = 604800;
			break;
		case 3:
			out = 2629743;
			break;
		}

		return out;
	}

	public String convertTicketTime(int validTime) {
		String out = "";

		switch (validTime) {
		case 0:
			out = "One Hour";
			break;
		case 1:
			out = "Day Ticket";
			break;
		case 2:
			out = "Weekly Ticket";
			break;
		case 3:
			out = "Monthly Ticket";
			break;

		}
		return out;
	}

	public String genHashCode(Ticket myTicket) {
		return Cryptography.hashString(myTicket.toString());
	}

	public String getStampTimeOnDB(Stamp myStamp) {
		String out = "";
		if (myStamp != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm",
					Locale.US);
			out = sdf.format(myStamp.getTime());
		} else {
			out = "Not Stamped";
		}
		return out;

	}
	
	public String getStampSigOnDB(Stamp myStamp) {
		String out = "";
		if (myStamp != null) {
			out = new String(Base64.encode(myStamp.getSig().getBytes()));
		} else {
			out = "Not Stamped";
		}
		return out;

	}

	public String getTicketRemainingTime(int validTime, Stamp myStamp) {
		long seconds = convertTicketTypeToHours(validTime);

		Date current = new Date();
		long time = current.getTime();
		Timestamp currentTimestamp = new Timestamp(time);

		long res = TimeUtil.compareTwoTimeStamps(currentTimestamp,
				myStamp.getTime());

		if (res < seconds) {
			return TimeUtil.formatIntoHHMMSS(seconds - res);
		}
		return "Expired";
	}

    public long getTicketRemainingTimeInLong(int validTime, Stamp myStamp) {
        long seconds = convertTicketTypeToHours(validTime);
        Date current = new Date();
        long time = current.getTime();
        Timestamp currentTimestamp = new Timestamp(time);

        long res = TimeUtil.compareTwoTimeStamps(currentTimestamp,
                myStamp.getTime());

        return (seconds -res) * 1000;
    }

	public boolean hasValidStamp(String strStamp) {
		if (strStamp.contains("No")) {
			return false;
		}
		return true;
	}

}

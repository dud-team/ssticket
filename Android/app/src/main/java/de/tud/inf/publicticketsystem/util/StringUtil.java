package de.tud.inf.publicticketsystem.util;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.util.Base64;

public class StringUtil {

	public static String asciiToHex(String ascii) {
		StringBuilder hex = new StringBuilder();

		for (int i = 0; i < ascii.length(); i++) {
			hex.append(Integer.toHexString(ascii.charAt(i)));
		}
		return hex.toString();
	}

	public String getDateTime() {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss", Locale.getDefault());
		Date date = new Date();
		return dateFormat.format(date);
	}

	public static String[] fixResponse(String str) {
		return str.split("\\|");

	}

	public static String formatDateFromServer(String str) {
		StringBuilder out = new StringBuilder();
		out.append(str.substring(0, 4)).append("-");
		out.append(str.substring(4, 6)).append("-");
		out.append(str.substring(6, 8)).append(" ");
		out.append(str.substring(8, 10)).append(":");
		out.append(str.substring(10, 12)).append(":");
		out.append(str.substring(12, 14));
		return out.toString();
	}

	public static String Base64Encode(String hash) {
		byte[] data = null;
		try {
			data = hash.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		return Base64.encodeToString(data, Base64.DEFAULT);
	}

}
